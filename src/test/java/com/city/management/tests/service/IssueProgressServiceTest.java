package com.city.management.tests.service;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.city.management.web.dto.IssueProgressDto;
import org.springframework.core.convert.ConversionService;
import com.city.management.persistence.dao.IssueProgressDao;
import com.city.management.persistence.entity.IssueProgress;
import com.city.management.service.impl.IssueProgressServiceImpl;

import java.util.List;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class IssueProgressServiceTest {

    @Mock
    private IssueProgressDao issueProgressDao;

    @Mock
    private ConversionService conversionService;


    @InjectMocks
    private IssueProgressServiceImpl issueProgressServiceImpl;

    private final IssueProgress existingIssueProgress = IssueProgress.builder()
            .description("service test existing issue progress")
            .build();
    private final IssueProgressDto existingIssueProgressDto = IssueProgressDto.builder()
            .description("service test existing issue progress")
            .build();

    private final IssueProgress newIssueProgress = IssueProgress.builder()
            .description("service test new issue progress")
            .build();
    private final IssueProgressDto newIssueProgressDto = IssueProgressDto.builder()
            .description("service test new issue progress")
            .build();

    private final Long existingIssueProgressId = 1L;

    @Test
    void shouldFindByIdSuccess() {
        when(issueProgressDao.findById(existingIssueProgressId)).thenReturn(existingIssueProgress);
        when(conversionService.convert(existingIssueProgress, IssueProgressDto.class)).thenReturn(existingIssueProgressDto);

        assertThat(issueProgressServiceImpl.findById(existingIssueProgressId)).isEqualTo(existingIssueProgressDto);

        verify(issueProgressDao).findById(existingIssueProgressId);
        verify(conversionService).convert(existingIssueProgress, IssueProgressDto.class);
    }

    @Test
    void shouldSaveIssueProgressSuccess() {
        when(issueProgressDao.save(newIssueProgress)).thenReturn(newIssueProgress);
        when(conversionService.convert(newIssueProgressDto, IssueProgress.class)).thenReturn(newIssueProgress);
        when(conversionService.convert(newIssueProgress, IssueProgressDto.class)).thenReturn(newIssueProgressDto);

        assertThat(issueProgressServiceImpl.save(newIssueProgressDto)).isEqualTo(newIssueProgressDto);

        verify(issueProgressDao).save(newIssueProgress);
        verify(conversionService).convert(newIssueProgressDto, IssueProgress.class);
        verify(conversionService).convert(newIssueProgress, IssueProgressDto.class);
    }

    @Test
    void shouldDeleteIssueProgressByIdSuccess() {
        issueProgressDao.deleteById(existingIssueProgressId);
        int issueProgressSize = 1;
        assertThat(issueProgressServiceImpl.findAll().size()).isEqualTo(issueProgressSize - 1);
    }

    @Test
    void shouldFindAllIssueProgressSuccess() {
        List<IssueProgress> issueProgressList = Collections.singletonList(existingIssueProgress);
        when(issueProgressDao.findAll()).thenReturn(issueProgressList);
        when(conversionService.convert(existingIssueProgress, IssueProgressDto.class)).thenReturn(existingIssueProgressDto);

        assertThat(issueProgressServiceImpl.findAll()).containsOnly(existingIssueProgressDto);

        verify(issueProgressDao).findAll();
        verify(conversionService).convert(existingIssueProgress, IssueProgressDto.class);
    }

    @Test
    void shouldUpdateByIdSuccess(){
        when(issueProgressDao.update(existingIssueProgressId, newIssueProgress)).thenReturn(newIssueProgress);
        when(conversionService.convert(newIssueProgress, IssueProgressDto.class)).thenReturn(newIssueProgressDto);
        when(conversionService.convert(newIssueProgressDto, IssueProgress.class)).thenReturn(newIssueProgress);

        assertThat(issueProgressServiceImpl.update(existingIssueProgressId, newIssueProgressDto)).isEqualTo(newIssueProgressDto);

        verify(issueProgressDao).update(existingIssueProgressId, newIssueProgress);
        verify(conversionService).convert(newIssueProgress, IssueProgressDto.class);
        verify(conversionService).convert(newIssueProgressDto, IssueProgress.class);

    }
}
