package com.city.management.tests.service;

import com.city.management.persistence.dao.IssueDao;
import com.city.management.persistence.entity.Issue;
import com.city.management.service.impl.IssueServiceImpl;
import com.city.management.web.dto.IssueDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

import java.util.Collections;
import java.util.List;

import static com.city.management.tests.helper.IssueUtil.INITIAL_ISSUE;
import static com.city.management.tests.helper.IssueUtil.INITIAL_ISSUE_DTO;
import static com.city.management.tests.helper.IssueUtil.UPDATED_ISSUE;
import static com.city.management.tests.helper.IssueUtil.UPDATED_ISSUE_DTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IssueServiceImplTest {

    private final static Long EXISTING_ISSUE_ID = 1L;

    private final static String EXISTING_ISSUE_TITLE = "Some title";

    @Mock
    private IssueDao issueDaoMock;

    @Mock
    private ConversionService conversionService;

    @InjectMocks
    private IssueServiceImpl issueServiceImpl;

    @Test
    void saveIssue() {
        when(issueDaoMock.save(INITIAL_ISSUE)).thenReturn(INITIAL_ISSUE);
        when(conversionService.convert(INITIAL_ISSUE_DTO, Issue.class)).thenReturn(INITIAL_ISSUE);
        when(conversionService.convert(INITIAL_ISSUE, IssueDto.class)).thenReturn(INITIAL_ISSUE_DTO);

        assertThat(issueServiceImpl.save(INITIAL_ISSUE_DTO)).isEqualTo(INITIAL_ISSUE_DTO);

        verify(issueDaoMock).save(INITIAL_ISSUE);
        verify(conversionService).convert(INITIAL_ISSUE_DTO, Issue.class);
        verify(conversionService).convert(INITIAL_ISSUE, IssueDto.class);
    }

    @Test
    void findAllIssues() {
        final List<Issue> issueList = Collections.singletonList(INITIAL_ISSUE);
        when(issueDaoMock.findAll()).thenReturn(issueList);
        when(conversionService.convert(INITIAL_ISSUE, IssueDto.class)).thenReturn(INITIAL_ISSUE_DTO);

        assertThat(issueServiceImpl.findAllIssues()).containsOnly(INITIAL_ISSUE_DTO);

        verify(issueDaoMock).findAll();
        verify(conversionService).convert(INITIAL_ISSUE, IssueDto.class);
    }

    @Test
    void findOneIssueById() {
        when(issueDaoMock.findById(EXISTING_ISSUE_ID)).thenReturn(INITIAL_ISSUE);
        when(conversionService.convert(INITIAL_ISSUE, IssueDto.class)).thenReturn(INITIAL_ISSUE_DTO);

        assertThat(issueServiceImpl.findById(EXISTING_ISSUE_ID)).isEqualTo(INITIAL_ISSUE_DTO);

        verify(issueDaoMock).findById(EXISTING_ISSUE_ID);
        verify(conversionService).convert(INITIAL_ISSUE, IssueDto.class);
    }

    @Test
    void findIssueByTitle() {
        when(issueDaoMock.findByTitle(EXISTING_ISSUE_TITLE)).thenReturn(INITIAL_ISSUE);
        when(conversionService.convert(INITIAL_ISSUE, IssueDto.class)).thenReturn(INITIAL_ISSUE_DTO);

        assertThat(issueServiceImpl.findByTitle(EXISTING_ISSUE_TITLE)).isEqualTo(INITIAL_ISSUE_DTO);

        verify(issueDaoMock).findByTitle(EXISTING_ISSUE_TITLE);
        verify(conversionService).convert(INITIAL_ISSUE, IssueDto.class);
    }

    @Test
    void updateIssueById() {
        when(issueDaoMock.update(EXISTING_ISSUE_ID, UPDATED_ISSUE)).thenReturn(UPDATED_ISSUE);
        when(conversionService.convert(UPDATED_ISSUE_DTO, Issue.class)).thenReturn(UPDATED_ISSUE);
        when(conversionService.convert(UPDATED_ISSUE, IssueDto.class)).thenReturn(UPDATED_ISSUE_DTO);

        assertThat(issueServiceImpl.updateById(EXISTING_ISSUE_ID, UPDATED_ISSUE_DTO)).isEqualTo(UPDATED_ISSUE_DTO);

        verify(issueDaoMock).update(EXISTING_ISSUE_ID, UPDATED_ISSUE);
        verify(conversionService).convert(UPDATED_ISSUE_DTO, Issue.class);
        verify(conversionService).convert(UPDATED_ISSUE, IssueDto.class);
    }

    @Test
    void deleteIssueById() {
        doNothing().when(issueDaoMock).deleteById(EXISTING_ISSUE_ID);
        assertThatCode(() -> issueServiceImpl.deleteById(EXISTING_ISSUE_ID)).doesNotThrowAnyException();
    }

}
