package com.city.management.tests.service;

import com.city.management.persistence.dao.ResourceDao;
import com.city.management.persistence.entity.Resource;
import com.city.management.service.impl.ResourceServiceImpl;
import com.city.management.web.dto.ResourceDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

import java.util.Collections;
import java.util.List;

import static com.city.management.tests.helper.ResourceUtil.INITIAL_RESOURCE;
import static com.city.management.tests.helper.ResourceUtil.INITIAL_RESOURCE_DTO;
import static com.city.management.tests.helper.ResourceUtil.UPDATED_RESOURCE;
import static com.city.management.tests.helper.ResourceUtil.UPDATED_RESOURCE_DTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ResourceServiceTest {
    @Mock
    private ResourceDao resourceDao;

    @Mock
    private ConversionService conversionService;

    @InjectMocks
    private ResourceServiceImpl resourceService;

    private final static Long EXISTING_RESOURCE_ID = 1L;

    @Test
    public void testFindById() {
        when(resourceDao.findById(EXISTING_RESOURCE_ID)).thenReturn(INITIAL_RESOURCE);
        when(conversionService.convert(INITIAL_RESOURCE, ResourceDto.class)).thenReturn(INITIAL_RESOURCE_DTO);

        assertThat(resourceService.findById(EXISTING_RESOURCE_ID)).isEqualTo(INITIAL_RESOURCE_DTO);

        verify(resourceDao).findById(EXISTING_RESOURCE_ID);
        verify(conversionService).convert(INITIAL_RESOURCE, ResourceDto.class);
    }

    @Test
    public void testFindAll() {
        final List<Resource> expectedResources = Collections.singletonList(INITIAL_RESOURCE);
        when(resourceDao.findAll()).thenReturn(expectedResources);
        when(conversionService.convert(INITIAL_RESOURCE, ResourceDto.class)).thenReturn(INITIAL_RESOURCE_DTO);

        assertThat(resourceService.findAll()).containsOnly(INITIAL_RESOURCE_DTO);

        verify(resourceDao).findAll();
        verify(conversionService).convert(INITIAL_RESOURCE, ResourceDto.class);
    }

    @Test
    public void testSave() {
        when(resourceDao.save(INITIAL_RESOURCE)).thenReturn(INITIAL_RESOURCE);
        when(conversionService.convert(INITIAL_RESOURCE_DTO, Resource.class)).thenReturn(INITIAL_RESOURCE);
        when(conversionService.convert(INITIAL_RESOURCE, ResourceDto.class)).thenReturn(INITIAL_RESOURCE_DTO);

        assertThat(resourceService.save(INITIAL_RESOURCE_DTO)).isEqualTo(INITIAL_RESOURCE_DTO);

        verify(resourceDao).save(INITIAL_RESOURCE);
        verify(conversionService).convert(INITIAL_RESOURCE_DTO, Resource.class);
        verify(conversionService).convert(INITIAL_RESOURCE, ResourceDto.class);
    }

    @Test
    public void testUpdate() {
        when(resourceDao.update(EXISTING_RESOURCE_ID, UPDATED_RESOURCE)).thenReturn(UPDATED_RESOURCE);
        when(conversionService.convert(UPDATED_RESOURCE_DTO, Resource.class)).thenReturn(UPDATED_RESOURCE);
        when(conversionService.convert(UPDATED_RESOURCE, ResourceDto.class)).thenReturn(UPDATED_RESOURCE_DTO);

        assertThat(resourceService.update(EXISTING_RESOURCE_ID, UPDATED_RESOURCE_DTO)).isEqualTo(UPDATED_RESOURCE_DTO);

        verify(resourceDao).update(EXISTING_RESOURCE_ID, UPDATED_RESOURCE);
        verify(conversionService).convert(UPDATED_RESOURCE_DTO, Resource.class);
        verify(conversionService).convert(UPDATED_RESOURCE, ResourceDto.class);
    }

    @Test
    public void testDelete() {
        doNothing().when(resourceDao).deleteById(EXISTING_RESOURCE_ID);
        assertThatCode(() -> resourceService.deleteById(EXISTING_RESOURCE_ID)).doesNotThrowAnyException();
    }
}
