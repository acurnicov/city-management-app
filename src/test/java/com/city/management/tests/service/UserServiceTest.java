package com.city.management.tests.service;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.junit.jupiter.api.Test;
import com.city.management.web.dto.UserDto;
import com.city.management.enums.UserStatus;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.city.management.persistence.entity.User;
import com.city.management.persistence.dao.UserDao;
import com.city.management.tests.helper.UserUtilClass;
import com.city.management.service.impl.UserServiceImpl;
import org.springframework.core.convert.ConversionService;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doNothing;
import static org.mockito.ArgumentMatchers.any;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserDao userDao;

    @Mock
    private ConversionService conversionService;

    @InjectMocks
    private UserServiceImpl userService;

    // TODO : finish to implement
    /*
    @Test
    public void getAllTest() {
        final List<User> expectedUserList = TestUtilClass.getInitialUserList();
        final List<UserDto> expectedUserDtoList = TestUtilClass.convertUserDtoList(expectedUserList);

        when(userDao.findAll()).thenReturn(expectedUserList);
        when(conversionService.convert(expectedUserList, UserDto.class)).thenReturn(expectedUserDtoList);

        assertThat(userService.getAll()).containsOnly(expectedUserDtoList);

        verify(userDao).findAll();
        verify(conversionService).convert(expectedUserList, UserDto.class);
    }*/

    @Test
    public void saveNotExistUserTest() {
        User user = UserUtilClass.USER_2;
        user.setEmail("smith_john@gmail.com");
        final UserDto userDto = UserUtilClass.createUserDtoInstance(user);

        when(userDao.save(user)).thenReturn(user);
        when(conversionService.convert(user, UserDto.class)).thenReturn(userDto);
        when(conversionService.convert(userDto, User.class)).thenReturn(user);

        assertThat(userService.signUp(userDto)).isEqualTo(userDto);

        verify(userDao).save(user);
        verify(conversionService).convert(user, UserDto.class);
        verify(conversionService).convert(userDto, User.class);
    }

    @Test
    public void saveExistsUserTest() {
        final User user = UserUtilClass.USER_3;
    }

    @Test
    public void isEmailAlreadyUsedTest() {
        final User user = UserUtilClass.USER_4;
        when(userDao.isEmailAlreadyUsed(user.getEmail())).thenReturn(true);
        assertThat(userService.isEmailAlreadyUsed(user.getEmail())).isEqualTo(true);
    }

    @Test
    public void emailIsNotAlreadyUsedShouldReturnFalse() {
        User user = UserUtilClass.USER_4;
        user.setEmail("smith__john@gmail.com");

        when(userDao.isEmailAlreadyUsed(user.getEmail())).thenReturn(false);
        assertThat(userService.isEmailAlreadyUsed(user.getEmail())).isEqualTo(false);
    }

    @Test
    public void getByEmailTest() {
        final User userToFind = UserUtilClass.USER_4;
        final UserDto userDto = UserUtilClass.createUserDtoInstance(userToFind);

        when(userDao.findByEmail(userToFind.getEmail())).thenReturn(userToFind);
        when(conversionService.convert(userToFind, UserDto.class)).thenReturn(userDto);

        assertThat(userService.getByEmail(userToFind.getEmail())).isEqualTo(userDto);

        verify(userDao).findByEmail(userToFind.getEmail());
        verify(conversionService).convert(userToFind, UserDto.class);
    }

    @Test
    public void updateTest() {
        User userToUpdate = UserUtilClass.USER_2;
        userToUpdate.setFirstName("Michael");
        final UserDto userDto = UserUtilClass.createUserDtoInstance(userToUpdate);

        when(userDao.update(userToUpdate)).thenReturn(userToUpdate);
        when(conversionService.convert(userDto, User.class)).thenReturn(userToUpdate);
        when(conversionService.convert(userToUpdate, UserDto.class)).thenReturn(userDto);

        assertThat(userService.update(userDto)).isEqualTo(userDto);

        verify(userDao).update(userToUpdate);
        verify(conversionService).convert(userDto, User.class);
        verify(conversionService).convert(userToUpdate, UserDto.class);
    }

    @Test
    public void changeStatusTest() {
        User userToUpdate = UserUtilClass.USER_1;
        userToUpdate.setStatus(UserStatus.ACTIVE);
        final UserDto userDto = UserUtilClass.createUserDtoInstance(userToUpdate);

        when(userDao.update(userToUpdate)).thenReturn(userToUpdate);
        when(conversionService.convert(userDto, User.class)).thenReturn(userToUpdate);
        when(conversionService.convert(userToUpdate, UserDto.class)).thenReturn(userDto);

        assertThat(userService.update(userDto)).isEqualTo(userDto);

        verify(userDao).update(userToUpdate);
        verify(conversionService).convert(userDto, User.class);
        verify(conversionService).convert(userToUpdate, UserDto.class);
    }

    @Test
    public void destroyExistingUserByEmailTest() {
        final User userToDestroy = UserUtilClass.USER_5;

        doNothing().when(userDao).destroyByEmail(userToDestroy.getEmail());
        assertThatCode(() -> userService.destroyByEmail(userToDestroy.getEmail())).doesNotThrowAnyException();
    }

    // TODO
    @Test
    public void destroyNonExistingUserByEmailShouldThrowApiException() {

    }

}
