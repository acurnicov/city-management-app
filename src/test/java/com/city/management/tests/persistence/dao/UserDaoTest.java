package com.city.management.tests.persistence.dao;

import java.util.List;
import org.junit.jupiter.api.Test;
import com.city.management.enums.UserStatus;
import com.city.management.exception.ApiException;
import com.city.management.persistence.entity.User;
import com.city.management.persistence.dao.UserDao;
import com.city.management.tests.helper.UserUtilClass;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void fieldValueExistsTest() {
        final User dbUser = UserUtilClass.USER_1;

        final boolean userExistsByFirstName = userDao.fieldValueExists(User.DBColumn.FIRST_NAME, dbUser.getFirstName());
        assertTrue(userExistsByFirstName);

        final boolean userExistsByLastName = userDao.fieldValueExists(User.DBColumn.LAST_NAME, dbUser.getLastName());
        assertTrue(userExistsByLastName);

        final boolean userExistsByEmail = userDao.fieldValueExists(User.DBColumn.EMAIL, dbUser.getEmail());
        assertTrue(userExistsByEmail);

        final boolean userExistsByPhoneNumber = userDao.fieldValueExists(User.DBColumn.PHONE_NUMBER, dbUser.getPhoneNumber());
        assertTrue(userExistsByPhoneNumber);
    }

    @Test
    public void findAllTest() {
        final List<User> userList = userDao.findAll();

        assertNotNull(userList);
        assertFalse(userList.isEmpty());
    }

    @Test
    public void saveNotExistUserTest() {
        User user = UserUtilClass.USER_2;
        user.setEmail("smith_john@gmail.com");

        final User savedUser = userDao.save(user);
        assertNotNull(savedUser);
        assertNotNull(savedUser.getId());
        assertEquals(user, savedUser);
    }

    @Test
    public void saveExistsUserTest() {
        final User user = UserUtilClass.USER_3;

        assertThrows(ApiException.class, () -> { userDao.save(user); });
    }

    @Test
    public void findByEmailTest() {
        final User user = UserUtilClass.USER_4;

        final User dbUser4 = userDao.findByEmail(user.getEmail());
        assertNotNull(dbUser4);
        assertEquals(user, dbUser4);
    }

    @Test
    public void isEmailAlreadyUsedTest() {
        final User user = UserUtilClass.USER_4;

        assertTrue(userDao.isEmailAlreadyUsed(user.getEmail()));
    }

    @Test
    public void updateTest() {
        User userToUpdate = UserUtilClass.USER_2;

        userToUpdate.setLastName("Doe");
        final User updatedUser = userDao.update(userToUpdate);
        assertNotNull(updatedUser);
        assertNotNull(updatedUser.getId());

        assertEquals("Doe", updatedUser.getLastName());
    }

    @Test
    public void changeStatusTest() {
        final User user1 = UserUtilClass.USER_1;
        assertEquals(UserStatus.REGISTERED, user1.getStatus());

        final User updatedUser = userDao.changeStatus(user1.getEmail(), UserStatus.ACTIVE.getStatus());
        assertEquals(UserStatus.ACTIVE, updatedUser.getStatus());
    }

    @Test
    public void destroyTest() {
        final User user5 = UserUtilClass.USER_5;

        userDao.destroyByEmail(user5.getEmail());

        assertThrows(ApiException.class, () -> { userDao.findByEmail(user5.getEmail()); });
    }

}
