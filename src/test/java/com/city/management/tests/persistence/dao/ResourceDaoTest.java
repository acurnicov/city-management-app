package com.city.management.tests.persistence.dao;

import com.city.management.persistence.dao.ResourceDao;
import com.city.management.persistence.entity.Resource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.city.management.tests.helper.ResourceUtil.INITIAL_RESOURCE;
import static com.city.management.tests.helper.ResourceUtil.UPDATED_RESOURCE;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class ResourceDaoTest {
    @Autowired
    private ResourceDao resourceDao;

    private static Long INITIAL_RESOURCE_ID;
    private static final int INITIAL_RESOURCES_SIZE = 1;

    @BeforeEach
    public void insertResources() {
        INITIAL_RESOURCE_ID = resourceDao.save(INITIAL_RESOURCE).getId();
    }

    @AfterEach
    public void clearResources() {
        resourceDao.findAll().forEach(resource -> resourceDao.deleteById(resource.getId()));
    }

    @Test
    public void testFindById() {
        final Resource foundResource = resourceDao.findById(INITIAL_RESOURCE_ID);

        assertNotNull(foundResource.getId());
        assertArrayEquals(INITIAL_RESOURCE.getResource(), foundResource.getResource());
        assertEquals(INITIAL_RESOURCE.getResourceType(), foundResource.getResourceType());
        assertEquals(INITIAL_RESOURCE_ID, foundResource.getId());
    }

    @Test
    public void testFindAll() {
        final List<Resource> foundResources = resourceDao.findAll();

        assertNotNull(foundResources);
        assertEquals(INITIAL_RESOURCE.getResourceType(), foundResources.get(0).getResourceType());
        assertArrayEquals(INITIAL_RESOURCE.getResource(), foundResources.get(0).getResource());
    }

    @Test
    public void testSave() {
        final Resource savedResource = resourceDao.save(INITIAL_RESOURCE);
        final int newSize = resourceDao.findAll().size();

        assertNotNull(savedResource);
        assertNotNull(savedResource.getCreatedAt());
        assertEquals(savedResource, savedResource);
        assertEquals(INITIAL_RESOURCES_SIZE + 1, newSize);
    }

    @Test
    public void testUpdate() {
        final Resource updatedResource = resourceDao.update(INITIAL_RESOURCE_ID, UPDATED_RESOURCE);
        final int size = resourceDao.findAll().size();

        assertNotEquals(INITIAL_RESOURCE, updatedResource);
        assertEquals(INITIAL_RESOURCES_SIZE, size);
        assertEquals(UPDATED_RESOURCE.getResourceType(), updatedResource.getResourceType());
        assertArrayEquals(UPDATED_RESOURCE.getResource(), updatedResource.getResource());
    }

    @Test
    public void testDelete() {
        resourceDao.deleteById(INITIAL_RESOURCE_ID);

        final int newSize = resourceDao.findAll().size();

        assertEquals(INITIAL_RESOURCES_SIZE - 1, newSize);
    }
}
