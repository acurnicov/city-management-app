package com.city.management.tests.persistence.dao;

import com.city.management.persistence.dao.IssueDao;
import com.city.management.persistence.entity.Issue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
@Transactional
class IssueDaoImplTest {

    @Autowired
    private IssueDao issueDao;

    @BeforeEach
    void insertDataIntoDB() {
        final String issueTitle = "Some title1";
        final Integer issueUpVote = 1;
        final Integer issueDownVote = 0;
        final String issueAddress = "Some address1";
        final String issueDescription = "Some description1";

        Issue issue = Issue.builder()
                .title(issueTitle)
                .upVote(issueUpVote)
                .downVote(issueDownVote)
                .address(issueAddress)
                .description(issueDescription)
                .build();
        issueDao.save(issue);
    }

    @AfterEach
    void deleteAllFromDB() {
        issueDao.findAll().forEach(issue -> issueDao.deleteById(issue.getId()));
    }

    @Test
    @Rollback
    void shouldSaveIssueSuccess() {
        final String issueTitle = "Some title2";
        final Integer issueUpVote = 2;
        final Integer issueDownVote = 1;
        final String issueAddress = "Some address2";
        final String issueDescription = "Some description2";

        final Issue issue = Issue.builder()
                .title(issueTitle)
                .upVote(issueUpVote)
                .downVote(issueDownVote)
                .address(issueAddress)
                .description(issueDescription)
                .build();

        Issue savedIssue = issueDao.save(issue);

        assertNotNull(savedIssue);
        assertNotNull(savedIssue.getId());
        assertEquals(issue, savedIssue);
    }

    @Test
    @Rollback
    void shouldUpdateIssueByIdSuccess() {
        final Long id = issueDao.findAll().get(0).getId();
        final String updateTitle = "Another title";
        final Integer updateUpVote = 2;
        final Integer updateDownVote = 1;
        final String updateDescription = "Another description";

        Issue issueFromDB = issueDao.findById(id);
        issueFromDB.setTitle(updateTitle);
        issueFromDB.setUpVote(updateUpVote);
        issueFromDB.setDownVote(updateDownVote);
        issueFromDB.setDescription(updateDescription);

        final Issue updatedIssue = issueDao.update(issueFromDB.getId(), issueFromDB);

        assertNotNull(updatedIssue);
        assertNotNull(updatedIssue.getId());
        assertEquals(updateTitle, updatedIssue.getTitle());
        assertEquals(updateUpVote, updatedIssue.getUpVote());
        assertEquals(updateDownVote, updatedIssue.getDownVote());
        assertEquals(updateDescription, updatedIssue.getDescription());
    }

    @Test
    @Rollback
    void shouldFindAllIssuesSuccess() {
        final List<Issue> issueList = issueDao.findAll();

        assertNotNull(issueList);
        assertEquals(1, issueList.size());
        assertFalse(issueList.isEmpty());
    }

    @Test
    @Rollback
    void shouldFindIssueByIdSuccess() {
        final Long id = issueDao.findAll().get(0).getId();
        final String issueTitle = "Some title1";
        final Integer issueUpVote = 1;
        final Integer issueDownVote = 0;
        final String issueAddress = "Some address1";
        final String issueDescription = "Some description1";

        Issue issueFromDB = issueDao.findById(id);

        assertNotNull(issueDao.findById(id));
        assertEquals(issueTitle, issueFromDB.getTitle());
        assertEquals(issueUpVote, issueFromDB.getUpVote());
        assertEquals(issueDownVote, issueFromDB.getDownVote());
        assertEquals(issueAddress, issueFromDB.getAddress());
        assertEquals(issueDescription, issueFromDB.getDescription());
    }

    @Test
    @Rollback
    void shouldFindIssueByTitleSuccess() {
        final String issueTitle = "Some title2";
        final Integer issueUpVote = 2;
        final Integer issueDownVote = 1;
        final String issueAddress = "Some address2";
        final String issueDescription = "Some description2";

        final Issue issue = Issue.builder()
                .title(issueTitle)
                .upVote(issueUpVote)
                .downVote(issueDownVote)
                .address(issueAddress)
                .description(issueDescription)
                .build();

        issueDao.save(issue);

        assertNotNull(issueDao.findByTitle(issueTitle));
        assertEquals(issueTitle, issueDao.findByTitle(issueTitle).getTitle());
    }

    @Test
    @Rollback
    void shouldDeleteIssueByIdSuccess() {
        final Long id = issueDao.findAll().get(0).getId();

        assertEquals(1, issueDao.findAll().size());

        issueDao.deleteById(id);

        assertEquals(0, issueDao.findAll().size());
    }
}
