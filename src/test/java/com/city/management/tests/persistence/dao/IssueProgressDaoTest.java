package com.city.management.tests.persistence.dao;

import org.junit.jupiter.api.Test;
import com.city.management.enums.IssueStatus;
import com.city.management.persistence.dao.IssueProgressDao;
import org.springframework.boot.test.context.SpringBootTest;
import com.city.management.persistence.entity.IssueProgress;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
class IssueProgressDaoTest {

    @Autowired
    private IssueProgressDao issueProgressDao;

    @Test
    void shouldUpdateIssueProgressSuccess() {

        final IssueProgress SAVE_ISSUE_PROGRESS = IssueProgress.builder()
                .description("saved issue progress")
                .build();

        final IssueProgress ISSUE_PROGRESS = IssueProgress.builder()
                .description(" updated test description")
                .status(IssueStatus.IN_PROGRESS)
                .build();

        IssueProgress savedIssueProgress = issueProgressDao.save(SAVE_ISSUE_PROGRESS);
        IssueProgress updatedIssueProgress = issueProgressDao.update(savedIssueProgress.getId(), ISSUE_PROGRESS);

        assertNotNull(updatedIssueProgress);
        assertEquals(ISSUE_PROGRESS.getDescription(), updatedIssueProgress.getDescription());
        assertEquals(ISSUE_PROGRESS.getStatus(), updatedIssueProgress.getStatus());
        assertNotEquals(updatedIssueProgress.getCreatedAt(), updatedIssueProgress.getUpdatedAt());
    }

    @Test
    void shouldDeleteIssueProgressSuccess() {
        final IssueProgress ISSUE_PROGRESS = IssueProgress.builder()
                .description(" deleted test description")
                .build();
        final IssueProgress savedIssueProgress = issueProgressDao.save(ISSUE_PROGRESS);
        issueProgressDao.deleteById(savedIssueProgress.getId());

        final IssueProgress deletedIssueProgress = issueProgressDao.findById(savedIssueProgress.getId());
        assertNotNull(deletedIssueProgress);
        assertEquals(IssueStatus.ABORTED, deletedIssueProgress.getStatus());
        assertEquals(ISSUE_PROGRESS.getDescription(), deletedIssueProgress.getDescription());
    }

    @Test
    void shouldSaveIssueProgressSuccess() {
        final IssueProgress ISSUE_PROGRESS = IssueProgress.builder()
                .description(" save test description")
                .build();
        IssueProgress savedIssueProgress = issueProgressDao.save(ISSUE_PROGRESS);

        assertNotNull(savedIssueProgress);
        assertEquals(ISSUE_PROGRESS.getDescription(), savedIssueProgress.getDescription());
    }

    @Test
    void shouldFindByIdIssueProgressSuccess() {

        final IssueProgress ISSUE_PROGRESS = IssueProgress.builder()
                .description("findById")
                .status(IssueStatus.valueOf("QUEUED"))
                .build();

        IssueProgress foundIssueProgress = issueProgressDao.findById(issueProgressDao.save(ISSUE_PROGRESS).getId());

        assertNotNull(foundIssueProgress);
        assertEquals(ISSUE_PROGRESS.getDescription(), foundIssueProgress.getDescription());
        assertEquals(ISSUE_PROGRESS.getStatus(), foundIssueProgress.getStatus());
    }

    @Test
    void shouldFindAllIssueProgressSuccess() {

        final List<IssueProgress> LIST_ISSUE_PROGRESS = new ArrayList<>();
        final IssueProgress ISSUE_PROGRESS_1 = IssueProgress.builder()
                .description("test description1")
                .status(IssueStatus.valueOf("QUEUED"))
                .build();

        final IssueProgress ISSUE_PROGRESS_2 = IssueProgress.builder()
                .description("test description2")
                .status(IssueStatus.valueOf("QUEUED"))
                .build();
        issueProgressDao.save(ISSUE_PROGRESS_1);
        issueProgressDao.save(ISSUE_PROGRESS_2);

        LIST_ISSUE_PROGRESS.add(ISSUE_PROGRESS_1);
        LIST_ISSUE_PROGRESS.add(ISSUE_PROGRESS_2);

        ArrayList<IssueProgress> foundALLIssueProgress = new ArrayList<>(issueProgressDao.findAll());

        assertNotNull(foundALLIssueProgress);
        assertEquals(LIST_ISSUE_PROGRESS.get(0).getDescription(), foundALLIssueProgress.get(0).getDescription());
        assertEquals(LIST_ISSUE_PROGRESS.get(0).getStatus(), foundALLIssueProgress.get(0).getStatus());

        assertEquals(LIST_ISSUE_PROGRESS.get(1).getDescription(), foundALLIssueProgress.get(1).getDescription());
        assertEquals(LIST_ISSUE_PROGRESS.get(1).getStatus(), foundALLIssueProgress.get(1).getStatus());
    }
}
