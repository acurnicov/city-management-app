package com.city.management.tests.helper;

import java.util.List;
import java.util.Arrays;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import java.time.OffsetDateTime;
import java.util.stream.Collectors;
import com.city.management.web.dto.UserDto;
import com.city.management.enums.UserStatus;
import com.city.management.persistence.entity.User;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UserUtilClass {

    public static User USER_1 = createUserInstance(
            1L, "John", "Doe", "doe.john@gmail.com", "john.doe",
            "+1-541-754-3010", "Time Square 100", UserStatus.REGISTERED,
            OffsetDateTime.parse("2019-11-11T15:25:00Z")
    );
    public static User USER_2 = createUserInstance(
            2L, "John", "Smith", "smith.john@gmail.com", "john.smith",
            "+1-943-150-3610", "Joan Romero 23", UserStatus.REGISTERED,
            OffsetDateTime.parse("2019-11-12T11:10:00Z")
    );
    public static User USER_3 = createUserInstance(
            3L, "Abigail", "Brown", "br.abigail@yahoo.com",
            "br.abigail", "+1-341-521-0420", "Hedy Greene 3/2", UserStatus.ACTIVE,
            OffsetDateTime.parse("2019-11-12T12:00:00Z")
    );
    public static User USER_4 = createUserInstance(
            4L, "Rose", "Duncan", "dn.rrose@gmail.com",
            "dn.rose", "+1-911-911-0911", "Rebecca Chambers 11", UserStatus.REGISTERED,
            OffsetDateTime.parse("2019-11-12T18:20:00Z")
    );
    public static User USER_5 = createUserInstance(
            5L, "Michael", "Piper", "piper.michael@gmail.com",
            "michael.piper", "+1-200-404-0500", "Malcolm Long 66/1", UserStatus.ACTIVE,
            OffsetDateTime.parse("2019-11-13T05:05:05Z")
    );

    private static User createUserInstance(Long id, String firstName, String lastName, String email, String password,
                                           String phoneNumber, String address, UserStatus status, OffsetDateTime createdAt
    ) {
        return User.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .password(password)
                .phoneNumber(phoneNumber)
                .address(address)
                .status(status)
                .createdAt(createdAt)
                .build();
    }

    public static UserDto createUserDtoInstance(User user) {
        return UserDto.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .address(user.getAddress())
                .build();
    }

    public static List<User> getInitialUserList() {
        return Arrays.asList(USER_1, USER_2, USER_3, USER_4);
    }

    public static List<UserDto> convertUserDtoList(List<User> userList) {
        return userList.stream()
                .map(UserUtilClass::createUserDtoInstance)
                .collect(Collectors.toList());
    }

}
