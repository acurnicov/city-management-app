package com.city.management.tests.helper;

import com.city.management.persistence.entity.Issue;
import com.city.management.web.dto.IssueDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class IssueUtil {

    private static final String ISSUE_TITLE = "Some title";
    private static final String UPDATED_ISSUE_TITLE = "Updated title";
    private static final String ISSUE_ADDRESS = "Some address";
    private static final String UPDATED_ISSUE_ADDRESS = "Updated address";
    private static final String ISSUE_DESCRIPTION = "Some description";
    private static final String UPDATED_ISSUE_DESCRIPTION = "Updated description";

    public static final Issue INITIAL_ISSUE = newIssue(ISSUE_TITLE, ISSUE_ADDRESS, ISSUE_DESCRIPTION);
    public static final Issue UPDATED_ISSUE = newIssue(UPDATED_ISSUE_TITLE, UPDATED_ISSUE_ADDRESS, UPDATED_ISSUE_DESCRIPTION);

    public static final IssueDto INITIAL_ISSUE_DTO = newIssueDto(ISSUE_TITLE, ISSUE_ADDRESS, ISSUE_DESCRIPTION);
    public static final IssueDto UPDATED_ISSUE_DTO = newIssueDto(UPDATED_ISSUE_TITLE, UPDATED_ISSUE_ADDRESS, UPDATED_ISSUE_DESCRIPTION);

    private static Issue newIssue(String issueTitle, String issueAddress, String issueDescription) {
        return Issue.builder()
                .title(issueTitle)
                .address(issueAddress)
                .description(issueDescription)
                .build();
    }

    private static IssueDto newIssueDto(String issueTitle, String issueAddress, String issueDescription) {
        return IssueDto.builder()
                .title(issueTitle)
                .address(issueAddress)
                .description(issueDescription)
                .build();
    }

}