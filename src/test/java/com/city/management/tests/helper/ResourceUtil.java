package com.city.management.tests.helper;

import com.city.management.persistence.entity.Resource;
import com.city.management.web.dto.ResourceDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResourceUtil {
    private static final String RESOURCE_TYPE = "data:image/png;base64";
    private static final String UPDATED_RESOURCE_TYPE = "data:image/jpg;base64";
    private static final byte[] RESOURCE_BYTES = new byte[]{-1, 1};
    private static final byte[] UPDATED_RESOURCE_BYTES = new byte[]{-2, 2};
    private static final String RESOURCE_DTO_BASE64 = Base64.encodeBase64String(RESOURCE_BYTES);
    private static final String UPDATED_RESOURCE_DTO_BASE64 = Base64.encodeBase64String(UPDATED_RESOURCE_BYTES);

    public static final Resource INITIAL_RESOURCE = newResource(RESOURCE_BYTES, RESOURCE_TYPE);
    public static final Resource UPDATED_RESOURCE = newResource(UPDATED_RESOURCE_BYTES, UPDATED_RESOURCE_TYPE);

    public static final ResourceDto INITIAL_RESOURCE_DTO = newResourceDto(RESOURCE_DTO_BASE64, RESOURCE_TYPE);
    public static final ResourceDto UPDATED_RESOURCE_DTO = newResourceDto(UPDATED_RESOURCE_DTO_BASE64, UPDATED_RESOURCE_TYPE);

    private static Resource newResource(byte[] resource, String resourceType) {
        return Resource.builder()
                .resourceType(resourceType)
                .resource(resource)
                .build();
    }

    private static ResourceDto newResourceDto(String resource, String resourceType) {
        return ResourceDto.builder()
                .resourceType(resourceType)
                .resource(resource)
                .build();
    }
}