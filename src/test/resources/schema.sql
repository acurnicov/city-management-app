DROP TYPE IF EXISTS user_status;
CREATE TYPE user_status AS ENUM
(
    'REGISTERED',
    'ACTIVE',
    'DELETED'
);

DROP TYPE IF EXISTS issue_status;
CREATE TYPE issue_status AS ENUM
(
    'QUEUED',
    'IN_PROGRESS',
    'ABORTED',
    'DONE',
    'FAILED'
);

DROP TABLE IF EXISTS t_user;
CREATE TABLE t_user
(
    id           INTEGER            NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name   VARCHAR(50)        NOT NULL,
    last_name    VARCHAR(50)        NOT NULL,
    email        VARCHAR(50) UNIQUE NOT NULL,
    password     VARCHAR(32)        NOT NULL,
    phone_number VARCHAR(30)        NOT NULL,
    address      VARCHAR(300)       NOT NULL,
    status       user_status                 DEFAULT 'REGISTERED',
    created_at   TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW()
);

DROP TABLE IF EXISTS t_comment;
CREATE TABLE t_comment
(
    id          INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    t_parent_id INTEGER                     DEFAULT NULL,
    message     TEXT,
    created_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW(),

    CONSTRAINT t_comment_parent_id_fk FOREIGN KEY (t_parent_id) REFERENCES t_comment (id)
);

DROP TABLE IF EXISTS t_resource;
CREATE TABLE t_resource
(
    id            INTEGER     NOT NULL AUTO_INCREMENT PRIMARY KEY,
    resource_type VARCHAR(32) NOT NULL,
    resource      BYTEA       NOT NULL,
    created_at    TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW()
);

DROP TABLE IF EXISTS t_issue;
CREATE TABLE t_issue
(
    id          INTEGER      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title       VARCHAR(100) NOT NULL,
    up_vote     INTEGER                     DEFAULT 0,
    down_vote   INTEGER                     DEFAULT 0,
    address     VARCHAR(300) NOT NULL,
    description TEXT,
    created_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW(),
    updated_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW(),

    CONSTRAINT t_issue_min_up_vote_value CHECK (up_vote > -1),
    CONSTRAINT t_issue_min_down_vote_value CHECK (down_vote > -1)
);

DROP TABLE IF EXISTS t_issue_progress;
CREATE TABLE t_issue_progress
(
    id          INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    description TEXT,
    t_issue_id  INTEGER NOT NULL,
    status      issue_status                DEFAULT 'QUEUED',
    created_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW(),
    updated_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT NOW(),

    CONSTRAINT t_issue_progress_t_issue_id_fk FOREIGN KEY (t_issue_id) REFERENCES t_issue (id)
);

DROP TABLE IF EXISTS t_issue_resource;
CREATE TABLE t_issue_resource
(
    id            INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    t_issue_id    INTEGER NOT NULL,
    t_resource_id INTEGER NOT NULL,

    CONSTRAINT t_i_resource_i_id_fk FOREIGN KEY (t_issue_id) REFERENCES t_issue (id),
    CONSTRAINT t_i_resource_r_id_fk FOREIGN KEY (t_resource_id) REFERENCES t_resource (id)
);

DROP TABLE IF EXISTS t_issue_comment;
CREATE TABLE t_issue_comment
(
    id           INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    t_issue_id   INTEGER NOT NULL,
    t_comment_id INTEGER NOT NULL,

    CONSTRAINT t_issue_id_fk FOREIGN KEY (t_issue_id) REFERENCES t_issue (id),
    CONSTRAINT t_comment_id_fk FOREIGN KEY (t_comment_id) REFERENCES t_comment (id)
);

DROP TABLE IF EXISTS t_issue_progress_comment;
CREATE TABLE t_issue_progress_comment
(
    id                  INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    t_issue_progress_id INTEGER NOT NULL,
    t_comment_id        INTEGER NOT NULL,

    CONSTRAINT t_issue_progress_comment_progress_fk FOREIGN KEY (t_issue_progress_id) REFERENCES t_issue_progress (id),
    CONSTRAINT t_issue_progress_comment_resource_fk FOREIGN KEY (t_comment_id) REFERENCES t_comment (id)
);

DROP TABLE IF EXISTS t_issue_progress_resource;
CREATE TABLE t_issue_progress_resource
(
    id                  INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    t_issue_progress_id INTEGER NOT NULL,
    t_resource_id       INTEGER NOT NULL,

    CONSTRAINT t_issue_progress_resource_progress_fk FOREIGN KEY (t_issue_progress_id) REFERENCES t_issue_progress (id),
    CONSTRAINT t_issue_progress_resource_resource_fk FOREIGN KEY (t_resource_id) REFERENCES t_resource (id)
);
