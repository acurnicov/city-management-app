-- t_user
INSERT
    INTO t_user(first_name, last_name, email, password, phone_number, address, status)
    VALUES ('John', 'Doe', 'doe.john@gmail.com', 'john.doe', '+1-541-754-3010', 'Time Square 100', 'REGISTERED');

INSERT
    INTO t_user(first_name, last_name, email, password, phone_number, address, status)
    VALUES ('John', 'Smith', 'smith.john@gmail.com', 'john.smith', '+1-943-150-3610', 'Joan Romero 23', 'REGISTERED');

INSERT
    INTO t_user(first_name, last_name, email, password, phone_number, address, status)
    VALUES ('Abigail', 'Brown', 'br.abigail@yahoo.com', 'br.abigail', '+1-341-521-0420', 'Hedy Greene 3/2', 'ACTIVE');

INSERT
    INTO t_user(first_name, last_name, email, password, phone_number, address, status)
    VALUES ('Rose', 'Duncan', 'dn.rrose@gmail.com', 'dn.rose', '+1-911-911-0911', 'Rebecca Chambers 11', 'REGISTERED');

INSERT
    INTO t_user(first_name, last_name, email, password, phone_number, address, status)
    VALUES ('Michael', 'Piper', 'piper.michael@gmail.com', 'michael.piper', '+1-200-404-0500', 'Malcolm Long 66/1', 'ACTIVE');


-- t_comment
INSERT
    INTO t_comment(message)
	VALUES ('Public comment is posted on the City Council Meeting Schedule published each Friday. To get the meeting schedule and agendas emailed to you each week, subscribe to updates and alerts.');

INSERT
    INTO t_comment(message)
	VALUES ('A 15-minute public comment period is held in committee meetings where proposed changes to the municipal code or policy are being discussed.');

INSERT
    INTO t_comment(message)
	VALUES ('Each speaker gets a maximum of two minutes to speak. Speakers will be shown a warning when 30 seconds are remaining and the committee chair will ask speakers to stop at the end of two minutes. ');

INSERT
    INTO t_comment(message)
	VALUES ('Speakers sign-up in the Council Conference Room.');

INSERT
    INTO t_comment(message)
	VALUES ('To make your comments more effective, reference the applicable criteria, policies or guidelines relevant to each specific type of application.');
