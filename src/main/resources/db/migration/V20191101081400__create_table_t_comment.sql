CREATE TABLE t_comment
(
    id          SERIAL PRIMARY KEY,
    t_parent_id INTEGER                  DEFAULT NULL,
    message     TEXT,
    created_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),

    CONSTRAINT t_comment_parent_id_fk FOREIGN KEY (t_parent_id) REFERENCES t_comment (id)
);

COMMENT ON TABLE t_comment IS 'Table with issue comment.';
COMMENT ON COLUMN t_comment.id IS 'Primary key of t_comment table.';
COMMENT ON COLUMN t_comment.t_parent_id IS 'Foreign key of first parent comment.';
COMMENT ON COLUMN t_comment.message IS 'Message of comment.';
COMMENT ON COLUMN t_comment.created_at IS 'Date when was created this comment.';