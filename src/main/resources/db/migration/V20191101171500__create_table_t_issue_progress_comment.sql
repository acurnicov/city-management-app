CREATE TABLE t_issue_progress_comment
(
    id                  SERIAL PRIMARY KEY,
    t_issue_progress_id INTEGER NOT NULL,
    t_comment_id       INTEGER NOT NULL,

    CONSTRAINT t_issue_progress_comment_progress_fk FOREIGN KEY (t_issue_progress_id) REFERENCES t_issue_progress (id),
    CONSTRAINT t_issue_progress_comment_resource_fk FOREIGN KEY (t_comment_id) REFERENCES t_comment (id)
);

COMMENT ON TABLE t_issue_progress_comment IS 'Issues progress comments table. Contains ids of issue progress id and their comment id.';
COMMENT ON COLUMN t_issue_progress_comment.id IS 'Primary key of t_issue_progress_comment table.';
COMMENT ON COLUMN t_issue_progress_comment.t_issue_progress_id IS 'Foreig key. References id of t_issue_progress table.';
COMMENT ON COLUMN t_issue_progress_comment.t_comment_id IS 'Foreig key. References id of t_comment table.';