CREATE TYPE user_status AS ENUM
(
    'REGISTERED',
    'ACTIVE',
    'DELETED'
);
