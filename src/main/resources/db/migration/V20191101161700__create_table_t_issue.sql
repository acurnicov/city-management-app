CREATE TABLE t_issue
(
    id          SERIAL PRIMARY KEY,
    title       VARCHAR(100) NOT NULL,
    up_vote     INTEGER                  DEFAULT 0,
    down_vote   INTEGER                  DEFAULT 0,
    address     VARCHAR(300) NOT NULL,
    description TEXT,
    created_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),

    CONSTRAINT t_issue_min_up_vote_value CHECK (up_vote > -1),
    CONSTRAINT t_issue_min_down_vote_value CHECK (down_vote > -1)
);

COMMENT ON TABLE t_issue IS 't_issue table.';
COMMENT ON COLUMN t_issue.id IS 'Primary key of t_issue table, AUTOINCREMENT.';
COMMENT ON COLUMN t_issue.up_vote IS 'Vote integer, default 0';
COMMENT ON COLUMN t_issue.down_vote IS 'Vote integer, default 0';
COMMENT ON COLUMN t_issue.address IS 'Max length 300, NOT NULL';
COMMENT ON COLUMN t_issue.created_at IS 'Current date when create table, TIMESTAMP';
COMMENT ON COLUMN t_issue.updated_at IS 'Current date when create table, TIMESTAMP';
COMMENT ON COLUMN t_issue.description IS 'Date when table was updated, TIMESTAMP';
