CREATE TYPE issue_status AS ENUM
(
    'QUEUED',
    'IN_PROGRESS',
    'ABORTED',
    'DONE',
    'FAILED'
);
