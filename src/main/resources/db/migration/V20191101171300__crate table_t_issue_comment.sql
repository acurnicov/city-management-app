CREATE TABLE t_issue_comment
(
    id           SERIAL PRIMARY KEY,
    t_issue_id   INTEGER NOT NULL,
    t_comment_id INTEGER NOT NULL,

    CONSTRAINT t_issue_id_fk FOREIGN KEY (t_issue_id) REFERENCES t_issue (id),
    CONSTRAINT t_comment_id_fk FOREIGN KEY (t_comment_id) REFERENCES t_comment (id)
);

COMMENT ON TABLE t_issue_comment IS 'Issues comments table. Contains ids of issues id and their comment id.';
COMMENT ON COLUMN t_issue_comment.id IS 'Primary key of t_issue_comment table.';
COMMENT ON COLUMN t_issue_comment.t_issue_id IS 'Foreig key. References id of t_issue table.';
COMMENT ON COLUMN t_issue_comment.t_comment_id IS 'Foreig key. References id of t_comment table.';