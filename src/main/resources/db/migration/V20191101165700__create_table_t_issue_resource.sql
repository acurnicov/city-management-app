CREATE TABLE t_issue_resource
(
    id            SERIAL PRIMARY KEY,
    t_issue_id    INTEGER NOT NULL,
    t_resource_id INTEGER NOT NULL,

    CONSTRAINT t_i_resource_i_id_fk FOREIGN KEY (t_issue_id) REFERENCES t_issue (id),
    CONSTRAINT t_i_resource_r_id_fk FOREIGN KEY (t_resource_id) REFERENCES t_resource (id)
);

COMMENT ON TABLE t_issue_resource IS 'The issue resource table; contains information about issue and its resources. An issue can have one or more resources.';
COMMENT ON COLUMN t_issue_resource.id IS 'Primary key of t_issue_resource table, not null value and is auto incremented.';
COMMENT ON COLUMN t_issue_resource.t_issue_id IS 'The reference to t_issue row from <t_issue> table.';
COMMENT ON COLUMN t_issue_resource.t_resource_id IS 'The reference to t_resource row from <t_resource> table.';
