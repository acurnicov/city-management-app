CREATE TABLE t_issue_progress_resource
(
    id                  SERIAL PRIMARY KEY,
    t_issue_progress_id INTEGER NOT NULL,
    t_resource_id       INTEGER NOT NULL,

    CONSTRAINT t_issue_progress_resource_progress_fk FOREIGN KEY (t_issue_progress_id) REFERENCES t_issue_progress (id),
    CONSTRAINT t_issue_progress_resource_resource_fk FOREIGN KEY (t_resource_id) REFERENCES t_resource (id)
);

COMMENT ON TABLE t_issue_progress_resource IS 'Issues progress resource table. Contains ids of issue progress id and their resource id.';
COMMENT ON COLUMN t_issue_progress_resource.id IS 'Primary key of t_issue_progress_resource table.';
COMMENT ON COLUMN t_issue_progress_resource.t_issue_progress_id IS 'Foreig key. References id of t_issue_progress table.';
COMMENT ON COLUMN t_issue_progress_resource.t_resource_id IS 'Foreig key. References id of t_resource table.';