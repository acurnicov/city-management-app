CREATE TABLE t_issue_progress
(
    id          SERIAL PRIMARY KEY,
    description TEXT,
    t_issue_id  INTEGER NOT NULL,
    status      issue_status             DEFAULT 'QUEUED',
    created_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at  TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),

    CONSTRAINT t_issue_progress_t_issue_id_fk FOREIGN KEY (t_issue_id) REFERENCES t_issue (id)
);

COMMENT ON TABLE t_issue_progress IS 'The issue progress table; contains information about issues.';
COMMENT ON COLUMN t_issue_progress.id IS 'Primary key of t_issue_progress table, not null value and is auto incremented.';
COMMENT ON COLUMN t_issue_progress.description IS 'Description/details of the issue progress.';
COMMENT ON COLUMN t_issue_progress.t_issue_id IS 'The reference to t_issue row from <t_issue> table.';
COMMENT ON COLUMN t_issue_progress.status IS 'The status of issue, default value <QUEUED>.';
COMMENT ON COLUMN t_issue_progress.created_at IS 'The current datetime in <UTC> TIME ZONE.';
COMMENT ON COLUMN t_issue_progress.updated_at IS 'The current datetime in <UTC> TIME ZONE.';
