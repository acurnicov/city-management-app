CREATE TABLE t_resource
(
    id         SERIAL PRIMARY KEY,
    resource_type VARCHAR(32) NOT NULL,
    resource   BYTEA NOT NULL,
    created_at TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')
);

COMMENT ON TABLE t_resource IS 't_resource table. References with locations table.';
COMMENT ON COLUMN t_resource.id IS 'Primary key of t_resource table, NOT NULL, AUTOINCREMENT.';
COMMENT ON COLUMN t_resource.resource_type IS 'Resource type';
COMMENT ON COLUMN t_resource.resource IS 'Field for photo/video';
COMMENT ON COLUMN t_resource.created_at IS 'The current datetime in <UTC> TIME ZONE.';