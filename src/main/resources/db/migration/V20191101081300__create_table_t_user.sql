CREATE TABLE t_user
(
    id              SERIAL          PRIMARY KEY,
    first_name      VARCHAR(50)     NOT NULL,
    last_name       VARCHAR(50)     NOT  NULL,
    email           VARCHAR(50)     UNIQUE  NOT NULL,
    password        VARCHAR(32)     NOT NULL,
    phone_number    VARCHAR(30)     NOT NULL,
    address         VARCHAR(300)    NOT NULL,
    status          user_status     DEFAULT 'REGISTERED',
    created_at      TIMESTAMP(6) WITH TIME ZONE DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')
);

COMMENT ON TABLE t_user IS 'Users table.';
COMMENT ON COLUMN t_user.id IS 'Primary key of t_user table.';
COMMENT ON COLUMN t_user.first_name IS 'First name of user.';
COMMENT ON COLUMN t_user.last_name IS 'Last name of user.';
COMMENT ON COLUMN t_user.email IS 'User email, and tis should be unique.';
COMMENT ON COLUMN t_user.password IS 'User password.';
COMMENT ON COLUMN t_user.phone_number IS 'User phone number.';
COMMENT ON COLUMN t_user.address IS 'User address where he/she live.';
COMMENT ON COLUMN t_user.status IS 'User account status.';
COMMENT ON COLUMN t_user.created_at IS 'Date when was created this account.';
