package com.city.management.config.web.dto.conversion;

import com.city.management.web.dto.converter.issueprogress.IssueProgressDtoToIssueProgressConverter;
import com.city.management.web.dto.converter.issueprogress.IssueProgressToIssueProgressDtoConverter;
import com.city.management.web.dto.converter.issueprogress.UpdateIssueProgressDtoToIssueProgressConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;
import org.springframework.context.annotation.Configuration;
import com.city.management.web.dto.converter.resource.ResourceConverter;
import org.springframework.core.convert.support.DefaultConversionService;
import com.city.management.web.dto.converter.user.UserToUserDtoConverter;
import com.city.management.web.dto.converter.user.UserDtoToUserConverter;
import com.city.management.web.dto.converter.resource.ResourceDtoConverter;
import com.city.management.web.dto.converter.issue.IssueDtoToIssueConverter;
import com.city.management.web.dto.converter.issue.IssueToIssueDtoConverter;
import com.city.management.web.dto.converter.user.SignUpUserRequestToUserDtoConverter;
import com.city.management.web.dto.converter.user.UpdateUserRequestToUserDtoConverter;
import com.city.management.web.dto.converter.user.ChangeUserStatusRequestToUserDtoConverter;

@Configuration
public class ConversionServiceConfig {

    @Bean
    public ConversionService conversionService() {
        DefaultConversionService service = new DefaultConversionService();

        service.addConverter(new UserToUserDtoConverter());
        service.addConverter(new UserDtoToUserConverter());
        service.addConverter(new SignUpUserRequestToUserDtoConverter());
        service.addConverter(new UpdateUserRequestToUserDtoConverter());
        service.addConverter(new ChangeUserStatusRequestToUserDtoConverter());

        service.addConverter(new ResourceConverter());
        service.addConverter(new ResourceDtoConverter());

        service.addConverter(new IssueDtoToIssueConverter());
        service.addConverter(new IssueToIssueDtoConverter());

        service.addConverter(new IssueProgressToIssueProgressDtoConverter());
        service.addConverter(new IssueProgressDtoToIssueProgressConverter());
        service.addConverter(new UpdateIssueProgressDtoToIssueProgressConverter());

        return service;
    }

}
