package com.city.management.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.city.management.validation.impl.PasswordMatchesValidator;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "com.city.management.web" })
public class MvcConfig {

    @Bean
    public PasswordMatchesValidator passwordMatchesValidator() {
        return new PasswordMatchesValidator();
    }

}