package com.city.management.config.data;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;

@Configuration
@ComponentScan("com.city.management")
@ConditionalOnResource(resources = {"classpath:application.yml"})
@PropertySource("classpath:application.yml")
public class JdbcConfig {

    @Bean
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

}
