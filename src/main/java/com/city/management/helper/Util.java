package com.city.management.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Util {

    public static final String RESOURCE_DELIMITER = ",";

}
