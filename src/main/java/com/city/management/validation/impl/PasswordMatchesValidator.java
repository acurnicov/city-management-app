package com.city.management.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.city.management.validation.PasswordMatches;
import com.city.management.web.request.user.SignUpUserRequest;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public boolean isValid(final Object object, final ConstraintValidatorContext context) {
        final SignUpUserRequest signUpUserRequest = (SignUpUserRequest) object;

        return signUpUserRequest.getPassword().equals(signUpUserRequest.getConfirmationPassword());
    }

}
