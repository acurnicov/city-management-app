package com.city.management.validation.impl;

import lombok.RequiredArgsConstructor;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.context.ApplicationContext;
import com.city.management.validation.PresentEntityItem;
import com.city.management.persistence.entity.FieldValueExists;

@RequiredArgsConstructor
public class PresentEntityItemValidator implements ConstraintValidator<PresentEntityItem, String> {

    private String fieldName;
    private FieldValueExists service;
    private final ApplicationContext applicationContext;

    @Override
    public void initialize(final PresentEntityItem constraintAnnotation) {
        final Class<? extends FieldValueExists> clazz = constraintAnnotation.service();
        this.fieldName = constraintAnnotation.fieldName();
        final String serviceQualifier = constraintAnnotation.serviceQualifier();

        this.service = serviceQualifier.equals("")
            ? this.applicationContext.getBean(clazz)
            : this.applicationContext.getBean(serviceQualifier, clazz);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {
        return this.service.fieldValueExists(this.fieldName, value);
    }
}
