package com.city.management.validation.impl;

import org.passay.*;
import java.util.Arrays;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.city.management.validation.ValidPassword;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        final PasswordValidator validator = new PasswordValidator(Arrays.asList(
            new LengthRule(8),
            new UppercaseCharacterRule(1),
            new LowercaseCharacterRule(1),
            new DigitCharacterRule(1), 
            new SpecialCharacterRule(1)
        ));
        final RuleResult result = validator.validate(new PasswordData(password));

        return result.isValid();
    }

}
