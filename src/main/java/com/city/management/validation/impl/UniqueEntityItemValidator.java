package com.city.management.validation.impl;

import lombok.RequiredArgsConstructor;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.context.ApplicationContext;
import com.city.management.validation.UniqueEntityItem;
import com.city.management.persistence.entity.FieldValueExists;

@RequiredArgsConstructor
public class UniqueEntityItemValidator implements ConstraintValidator<UniqueEntityItem, Object> {

    private String fieldName;
    private FieldValueExists service;
    private final ApplicationContext applicationContext;

    @Override
    public void initialize(final UniqueEntityItem constraintAnnotation) {
        final Class<? extends FieldValueExists> clazz = constraintAnnotation.service();
        this.fieldName = constraintAnnotation.fieldName();
        final String serviceQualifier = constraintAnnotation.serviceQualifier();

        this.service = serviceQualifier.equals("")
                ? this.applicationContext.getBean(clazz)
                : this.applicationContext.getBean(serviceQualifier, clazz);
    }

    @Override
    public boolean isValid(final Object object, final ConstraintValidatorContext constraintValidatorContext) {
        return !this.service.fieldValueExists(this.fieldName, object);
    }
}
