package com.city.management.validation.impl;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import javax.validation.ConstraintValidator;
import com.city.management.validation.ValueOfEnum;
import javax.validation.ConstraintValidatorContext;

public class ValueOfEnumValidator implements ConstraintValidator<ValueOfEnum, CharSequence> {

    private List<String> acceptedValues;

    @Override
    public void initialize(final ValueOfEnum annotation) {
        this.acceptedValues = Stream.of(annotation.enumClass().getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(final CharSequence value, final ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return this.acceptedValues.contains(value.toString());
    }

}
