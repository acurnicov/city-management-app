package com.city.management.enums;

public enum UserStatus {

    REGISTERED("REGISTERED"),
    ACTIVE("ACTIVE"),
    DELETED("DELETED");

    private String status;

    UserStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
