package com.city.management.enums;

public enum IssueStatus {

    QUEUED,
    IN_PROGRESS,
    ABORTED,
    DONE,
    FAILED

}
