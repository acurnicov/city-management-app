package com.city.management.exception;

public class ResourceNotSavedException extends RuntimeException {
    public ResourceNotSavedException(String message) {
        super(message);
    }
}
