package com.city.management.exception;

public class ResourceNotUpdatedException extends RuntimeException {
    public ResourceNotUpdatedException(String message) {
        super(message);
    }
}