package com.city.management.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Getter
@RequiredArgsConstructor
public enum ExceptionType {
    RESOURCE_NOT_FOUND("Resource not found", NOT_FOUND),
    RESOURCE_NOT_DELETED("Resource not deleted", INTERNAL_SERVER_ERROR),
    RESOURCE_NOT_UPDATED("Resource not updated", INTERNAL_SERVER_ERROR),
    RESOURCE_NOT_CREATED("Resource not created", INTERNAL_SERVER_ERROR),
    CLIENT_ERROR("Bad request", BAD_REQUEST),
    SERVER_ERROR("Server error", BAD_REQUEST);

    private final String message;
    private final HttpStatus httpStatus;
}
