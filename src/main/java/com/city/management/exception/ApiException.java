package com.city.management.exception;

import lombok.Getter;

@Getter
public class ApiException extends RuntimeException {
    private final ExceptionType exceptionType;

    public ApiException(ExceptionType exceptionType) {
        super(exceptionType.getMessage());
        this.exceptionType = exceptionType;
    }
}
