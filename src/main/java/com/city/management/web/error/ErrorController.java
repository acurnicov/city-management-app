package com.city.management.web.error;

import com.city.management.exception.ApiException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ErrorController extends ResponseEntityExceptionHandler {
    @ExceptionHandler(ApiException.class)
    protected ResponseEntity<Error> handleApiException(ApiException exception) {
        return new ResponseEntity<>(getErrorBodyResponse(exception), getErrorHttpStatus(exception));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        List<String> errors = exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        return handleExceptionInternal(exception, getErrorBodyResponse(errors), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private Error getErrorBodyResponse(ApiException exception) {
        return Error.builder().error(Collections.singletonList(exception.getMessage())).build();
    }

    private Error getErrorBodyResponse(List<String> exceptions) {
        return Error.builder().error(exceptions).build();
    }

    private HttpStatus getErrorHttpStatus(ApiException exception) {
        return exception.getExceptionType().getHttpStatus();
    }
}