package com.city.management.web.error;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class Error {
    private List<String> error;
}
