package com.city.management.web.request.user;

import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import java.time.OffsetDateTime;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;
import com.city.management.enums.UserStatus;
import javax.validation.constraints.NotBlank;
import com.city.management.service.UserService;
import com.city.management.persistence.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.city.management.validation.PresentEntityItem;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(of = {
    "firstName", "lastName", "email"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateUserRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    @Email
    @PresentEntityItem(
        service = UserService.class,
        fieldName = User.DBColumn.EMAIL,
        message = "There is no user with this email!"
    )
    private String email;

    private String password;
    private String confirmationPassword;

    @Size(min = 5)
    private String phoneNumber;

    @NotBlank
    private String address;

    private UserStatus status;

    private OffsetDateTime createdAt;

}
