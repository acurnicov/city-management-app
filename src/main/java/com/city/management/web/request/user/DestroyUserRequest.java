package com.city.management.web.request.user;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import com.city.management.service.UserService;
import com.city.management.persistence.entity.User;
import com.city.management.validation.PresentEntityItem;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class DestroyUserRequest {

    @NotBlank
    @Email
    @PresentEntityItem(
        service = UserService.class,
        fieldName = User.DBColumn.EMAIL,
        message = "There is no user with this email!"
    )
    private String email;

}
