package com.city.management.web.request.user;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Email;
import com.city.management.enums.UserStatus;
import javax.validation.constraints.NotBlank;
import com.city.management.service.UserService;
import com.city.management.validation.ValueOfEnum;
import com.city.management.persistence.entity.User;
import com.city.management.validation.PresentEntityItem;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ChangeUserStatusRequest {

    @NotBlank
    @Email
    @PresentEntityItem(
        service = UserService.class,
        fieldName = User.DBColumn.EMAIL,
        message = "There is no user with this email!"
    )
    private String email;

    @NotBlank
    @ValueOfEnum(enumClass = UserStatus.class)
    private String status;

}
