package com.city.management.web.dto.converter.user;

import com.city.management.web.dto.UserDto;
import org.springframework.stereotype.Component;
import com.city.management.persistence.entity.User;
import org.springframework.core.convert.converter.Converter;

@Component
public class UserToUserDtoConverter implements Converter<User, UserDto> {

    @Override
    public UserDto convert(final User user) {
        return UserDto.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .address(user.getAddress())
                .build();
    }

}
