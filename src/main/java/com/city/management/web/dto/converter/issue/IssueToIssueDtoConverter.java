package com.city.management.web.dto.converter.issue;

import com.city.management.persistence.entity.Issue;
import com.city.management.web.dto.IssueDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IssueToIssueDtoConverter implements Converter<Issue, IssueDto> {
    @Override
    public IssueDto convert(final Issue issue) {
        return IssueDto.builder()
                .title(issue.getTitle())
                .upVote(issue.getUpVote())
                .downVote(issue.getDownVote())
                .address(issue.getAddress())
                .description(issue.getDescription())
                .createdAt(issue.getCreatedAt())
                .updatedAt(issue.getUpdatedAt())
                .build();
    }

}
