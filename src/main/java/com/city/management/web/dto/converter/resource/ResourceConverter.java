package com.city.management.web.dto.converter.resource;

import com.city.management.persistence.entity.Resource;
import com.city.management.web.dto.ResourceDto;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.city.management.helper.Util.RESOURCE_DELIMITER;

@Component
public class ResourceConverter implements Converter<Resource, ResourceDto> {
    @Override
    public ResourceDto convert(final Resource resource) {
        byte[] resourceBytes = resource.getResource();
        String resourceBase64 = resource.getResourceType() + RESOURCE_DELIMITER + Base64.encodeBase64String(resourceBytes);

        return ResourceDto.builder()
                .resourceType(resource.getResourceType())
                .resource(resourceBase64)
                .build();
    }
}
