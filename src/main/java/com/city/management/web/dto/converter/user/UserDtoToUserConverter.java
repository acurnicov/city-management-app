package com.city.management.web.dto.converter.user;

import com.city.management.web.dto.UserDto;
import org.springframework.stereotype.Component;
import com.city.management.persistence.entity.User;
import org.springframework.core.convert.converter.Converter;

@Component
public class UserDtoToUserConverter implements Converter<UserDto, User> {

    @Override
    public User convert(final UserDto userDto) {
        return User.builder()
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .phoneNumber(userDto.getPhoneNumber())
                .address(userDto.getAddress())
                .build();
    }

}
