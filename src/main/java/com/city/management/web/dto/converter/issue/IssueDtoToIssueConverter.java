package com.city.management.web.dto.converter.issue;

import com.city.management.persistence.entity.Issue;
import com.city.management.web.dto.IssueDto;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

@Component
public class IssueDtoToIssueConverter implements Converter<IssueDto, Issue> {

    @Override
    public Issue convert(final IssueDto issueDto) {
        return Issue.builder()
                .title(issueDto.getTitle())
                .upVote(issueDto.getUpVote())
                .downVote(issueDto.getDownVote())
                .address(issueDto.getAddress())
                .description(issueDto.getDescription())
                .build();
    }
}
