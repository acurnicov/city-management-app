package com.city.management.web.dto.converter.user;

import com.city.management.web.dto.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import com.city.management.web.request.user.UpdateUserRequest;

@Component
public class UpdateUserRequestToUserDtoConverter implements Converter<UpdateUserRequest, UserDto> {

    @Override
    public UserDto convert(final UpdateUserRequest updateUserRequest) {
        return UserDto.builder()
                .firstName(updateUserRequest.getFirstName())
                .lastName(updateUserRequest.getLastName())
                .email(updateUserRequest.getEmail())
                .phoneNumber(updateUserRequest.getPhoneNumber())
                .address(updateUserRequest.getAddress())
                .build();
    }

}
