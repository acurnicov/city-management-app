package com.city.management.web.dto.converter.resource;

import com.city.management.persistence.entity.Resource;
import com.city.management.web.dto.ResourceDto;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.city.management.helper.Util.RESOURCE_DELIMITER;

@Component
public class ResourceDtoConverter implements Converter<ResourceDto, Resource> {
    private static final int RESOURCE_TYPE_INDEX = 0;
    private static final int RESOURCE_BASE64_INDEX = 1;

    @Override
    public Resource convert(final ResourceDto resourceDto) {
        final String[] resourceMeta = resourceDto.getResource().split(RESOURCE_DELIMITER);
        final String resourceType = resourceMeta[RESOURCE_TYPE_INDEX];
        final byte[] resourceBytes = Base64.decodeBase64(resourceMeta[RESOURCE_BASE64_INDEX]);

        return Resource.builder()
                .resourceType(resourceType)
                .resource(resourceBytes)
                .build();
    }
}
