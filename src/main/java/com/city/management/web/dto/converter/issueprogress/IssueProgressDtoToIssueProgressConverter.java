package com.city.management.web.dto.converter.issueprogress;

import com.city.management.web.dto.IssueProgressDto;
import org.springframework.core.convert.converter.Converter;
import com.city.management.persistence.entity.IssueProgress;

public class IssueProgressDtoToIssueProgressConverter implements Converter<IssueProgressDto, IssueProgress> {

    @Override
    public IssueProgress convert(final IssueProgressDto issueProgressDto) {
        return IssueProgress.builder()
                .description(issueProgressDto.getDescription())
                .build();
    }
}
