package com.city.management.web.dto.converter.user;

import com.city.management.web.dto.UserDto;
import com.city.management.enums.UserStatus;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import com.city.management.web.request.user.ChangeUserStatusRequest;

@Component
public class ChangeUserStatusRequestToUserDtoConverter implements Converter<ChangeUserStatusRequest, UserDto> {

    @Override
    public UserDto convert(final ChangeUserStatusRequest changeUserStatusRequest) {
        return UserDto.builder()
                .email(changeUserStatusRequest.getEmail())
                .status(UserStatus.valueOf(changeUserStatusRequest.getStatus()))
                .build();
    }

}
