package com.city.management.web.dto.converter.issueprogress;

import com.city.management.persistence.entity.IssueProgress;
import com.city.management.web.dto.IssueProgressDto;
import org.springframework.core.convert.converter.Converter;

public class UpdateIssueProgressDtoToIssueProgressConverter implements Converter<IssueProgressDto, IssueProgress> {
    @Override
    public IssueProgress convert(IssueProgressDto issueProgressDto) {
        return IssueProgress.builder()
                .description(issueProgressDto.getDescription())
                .status(issueProgressDto.getStatus())
                .build();
    }
}
