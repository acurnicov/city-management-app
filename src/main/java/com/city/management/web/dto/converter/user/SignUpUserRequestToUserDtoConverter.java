package com.city.management.web.dto.converter.user;

import com.city.management.web.dto.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import com.city.management.web.request.user.SignUpUserRequest;

@Component
public class SignUpUserRequestToUserDtoConverter implements Converter<SignUpUserRequest, UserDto> {

    @Override
    public UserDto convert(final SignUpUserRequest signUpUserRequest) {
        return UserDto.builder()
                .firstName(signUpUserRequest.getFirstName())
                .lastName(signUpUserRequest.getLastName())
                .email(signUpUserRequest.getEmail())
                .password(signUpUserRequest.getPassword())
                .phoneNumber(signUpUserRequest.getPhoneNumber())
                .address(signUpUserRequest.getAddress())
                .build();
    }

}
