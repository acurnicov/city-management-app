package com.city.management.web.dto;


import com.city.management.enums.IssueStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IssueProgressDto {

    private Long id;
    private String description;
    private IssueStatus status;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;

}
