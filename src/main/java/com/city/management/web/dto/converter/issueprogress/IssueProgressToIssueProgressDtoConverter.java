package com.city.management.web.dto.converter.issueprogress;

import com.city.management.web.dto.IssueProgressDto;
import org.springframework.core.convert.converter.Converter;
import com.city.management.persistence.entity.IssueProgress;

public class IssueProgressToIssueProgressDtoConverter implements Converter<IssueProgress, IssueProgressDto> {

    @Override
    public IssueProgressDto convert(final IssueProgress issueProgress) {
        return IssueProgressDto.builder()
                .id(issueProgress.getId())
                .description(issueProgress.getDescription())
                .status(issueProgress.getStatus())
                .createdAt(issueProgress.getCreatedAt())
                .updatedAt(issueProgress.getUpdatedAt())
                .build();
    }
}
