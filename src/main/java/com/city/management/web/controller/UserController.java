package com.city.management.web.controller;

import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import com.city.management.web.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.city.management.service.UserService;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.city.management.web.request.user.SignUpUserRequest;
import com.city.management.web.request.user.UpdateUserRequest;
import com.city.management.web.request.user.DestroyUserRequest;
import com.city.management.web.request.user.ChangeUserStatusRequest;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/users")
public class UserController {

    private final UserService userService;

    private final ConversionService conversionService;

    @GetMapping
    public ResponseEntity<List<UserDto>> getAll() {
        return ResponseEntity.ok().body(userService.getAll());
    }

    @PostMapping
    public ResponseEntity<UserDto> signUp(@RequestBody @Valid final SignUpUserRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.signUp(conversionService.convert(request, UserDto.class)));
    }

    @GetMapping("/email")
    public ResponseEntity<UserDto> get(@RequestParam("value") final String email) {
        return ResponseEntity.ok().body(userService.getByEmail(email));
    }

    @PutMapping
    public ResponseEntity<UserDto> update(@RequestBody @Valid final UpdateUserRequest request) {
        return ResponseEntity.ok().body(userService.update(conversionService.convert(request, UserDto.class)));
    }

    @PutMapping("/change-status")
    public ResponseEntity<UserDto> changeStatus(@RequestBody @Valid final ChangeUserStatusRequest request) {
        return ResponseEntity.ok().body(userService.changeStatus(conversionService.convert(request, UserDto.class)));
    }

    @DeleteMapping
    public ResponseEntity<Void> destroy(@RequestBody @Valid final DestroyUserRequest request) {
        userService.destroyByEmail(request.getEmail());

        return ResponseEntity.noContent().build();
    }

}
