package com.city.management.web.controller;

import com.city.management.service.IssueService;
import com.city.management.web.dto.IssueDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/issues")
public class IssueController {

    private IssueService issueService;

    @PostMapping
    public ResponseEntity<IssueDto> getIssue(@RequestBody IssueDto issueDto) {
        return ResponseEntity.ok().body(issueService.save(issueDto));
    }

    @GetMapping
    public ResponseEntity<List<IssueDto>> findAllIssues() {
        return ResponseEntity.ok(issueService.findAllIssues());
    }

    @GetMapping("/{id}")
    public ResponseEntity<IssueDto> findIssueById(@PathVariable Long id) {
        return ResponseEntity.ok().body(issueService.findById(id));
    }

    @GetMapping("/byTitle/{title}")
    public ResponseEntity<IssueDto> findIssueByTitle(@PathVariable String title) {
        return ResponseEntity.ok().body(issueService.findByTitle(title));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<IssueDto> updateIssueById(@PathVariable Long id, @RequestBody IssueDto issueDto) {
        return ResponseEntity.ok().body(issueService.updateById(id, issueDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        issueService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
