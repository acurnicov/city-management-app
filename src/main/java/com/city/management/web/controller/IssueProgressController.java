package com.city.management.web.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.city.management.web.dto.IssueProgressDto;
import com.city.management.service.IssueProgressService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/issue-progress")
public class IssueProgressController {

    private final IssueProgressService issueProgressService;

    @PostMapping
    public ResponseEntity<IssueProgressDto> save(@RequestBody final IssueProgressDto issueProgressDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(issueProgressService.save(issueProgressDto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<IssueProgressDto> findById(@PathVariable final Long id) {
        return ResponseEntity.ok().body(issueProgressService.findById(id));
    }

    @GetMapping
    public ResponseEntity<List<IssueProgressDto>> findAll() {
        return ResponseEntity.ok().body(issueProgressService.findAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        issueProgressService.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<IssueProgressDto> update(@PathVariable Long id, @RequestBody final IssueProgressDto issueProgressDto) {
        return ResponseEntity.ok().body(issueProgressService.update(id, issueProgressDto));

    }
}