package com.city.management.web.controller;

import com.city.management.service.ResourceService;
import com.city.management.web.dto.ResourceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/resources")
public class ResourceController {
    private final ResourceService resourceService;

    @GetMapping("/{id}")
    public ResponseEntity<ResourceDto> findById(@PathVariable final Long id) {
        return ResponseEntity.ok().body(resourceService.findById(id));
    }

    @GetMapping
    public ResponseEntity<List<ResourceDto>> findAll() {
        return ResponseEntity.ok().body(resourceService.findAll());
    }

    @PostMapping
    public ResponseEntity<ResourceDto> save(@Valid @RequestBody final ResourceDto resourceDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(resourceService.save(resourceDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Long id) {
        resourceService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResourceDto> update(@PathVariable final Long id, @Valid @RequestBody final ResourceDto resourceDto) {
        return ResponseEntity.ok().body(resourceService.update(id, resourceDto));
    }
}
