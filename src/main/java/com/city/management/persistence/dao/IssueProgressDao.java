package com.city.management.persistence.dao;

import java.util.List;
import com.city.management.persistence.entity.IssueProgress;

public interface IssueProgressDao {

    IssueProgress save(IssueProgress issueProgress);
    IssueProgress update(Long id, IssueProgress issueProgress);
    void deleteById(Long id);
    IssueProgress findById(Long id);
    List<IssueProgress> findAll();

}
