package com.city.management.persistence.dao;

import java.util.List;
import com.city.management.persistence.entity.Issue;

public interface IssueDao {

    Issue save(Issue issue);

    Issue update(Long id, Issue issue);

    List<Issue> findAll();

    Issue findById(Long id);

    Issue findByTitle(String title);

    void deleteById(Long id);

}
