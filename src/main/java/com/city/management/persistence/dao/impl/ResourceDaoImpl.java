package com.city.management.persistence.dao.impl;

import java.util.List;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.util.ArrayList;
import javax.sql.DataSource;
import java.sql.SQLException;
import lombok.extern.slf4j.Slf4j;
import java.sql.PreparedStatement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import com.city.management.exception.ApiException;
import com.city.management.exception.ExceptionType;
import com.city.management.persistence.dao.ResourceDao;
import com.city.management.persistence.entity.Resource;
import com.city.management.persistence.rowmapper.ResourceRowMapper;

@Slf4j
@Repository
@RequiredArgsConstructor
public class ResourceDaoImpl implements ResourceDao {
    private static final String FIND_BY_ID_QUERY = "select * from t_resource where id = ?";
    private static final String FIND_ALL_QUERY = "select * from t_resource";
    private static final String INSERT_QUERY = "insert into t_resource (resource_type, resource) values (?, ?)";
    private static final String DELETE_QUERY = "delete from t_resource where id = ?";
    private static final String UPDATE_QUERY = "update t_resource set resource_type = ?, resource = ? where id = ?";

    private final DataSource dataSource;
    private final ResourceRowMapper resourceRowMapper;

    @Override
    public Resource findById(final Long id) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY)) {
            preparedStatement.setLong(1, id);

            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resourceRowMapper.mapRow(resultSet, 0);
                }

                throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
            }
        } catch (final SQLException exception) {
            log.error("Resource with id " + id + " not found");
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public List<Resource> findAll() {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_QUERY)) {

            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                final List<Resource> resources = new ArrayList<>();
                while (resultSet.next()) {
                    resources.add(resourceRowMapper.mapRow(resultSet, 0));
                }
                return resources;
            }

        } catch (final SQLException exception) {
            log.error("Error finding all resources");
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public Resource save(final Resource resource) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, resource.getResourceType());
            preparedStatement.setBytes(2, resource.getResource());

            if (preparedStatement.executeUpdate() == 0) {
                throw new SQLException("Creating resource failed, no rows affected.");
            }

            try (final ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return this.findById(generatedKeys.getLong(1));
                }

                throw new SQLException("Creating resource failed, no ID obtained.");
            }
        } catch (final SQLException exception) {
            log.error("Error creating resource", exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_CREATED);
        }
    }

    @Override
    public void deleteById(final Long id) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (final SQLException exception) {
            log.error("Error deleting resource with id " + id);
            throw new ApiException(ExceptionType.RESOURCE_NOT_DELETED);
        }
    }

    @Override
    public Resource update(final Long id, final Resource resource) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, resource.getResourceType());
            preparedStatement.setBytes(2, resource.getResource());
            preparedStatement.setLong(3, id);

            if (preparedStatement.executeUpdate() == 0) {
                throw new SQLException("Error updating resource");
            }

            return findById(id);
        } catch (final SQLException exception) {
            log.error(exception.getMessage());
            throw new ApiException(ExceptionType.RESOURCE_NOT_UPDATED);
        }
    }
}