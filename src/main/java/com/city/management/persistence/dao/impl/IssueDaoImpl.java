package com.city.management.persistence.dao.impl;

import java.util.List;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.util.ArrayList;
import javax.sql.DataSource;
import java.sql.SQLException;
import lombok.extern.slf4j.Slf4j;
import java.sql.PreparedStatement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import com.city.management.exception.ApiException;
import com.city.management.exception.ExceptionType;
import com.city.management.persistence.dao.IssueDao;
import com.city.management.persistence.entity.Issue;
import com.city.management.persistence.rowmapper.IssueRowMapper;

@Slf4j
@Repository
@RequiredArgsConstructor
public class IssueDaoImpl implements IssueDao {

    private final String CREATE_ERROR_MESSAGE = "Error creating issue.";

    private final String UPDATE_ERROR_MESSAGE = "Error updating issue.";

    private final String FIND_ALL_ERROR_MESSAGE = "Error finding all issues.";

    private final String FIND_BY_ID_ERROR_MESSAGE = "Error finding issue by id.";

    private final String DELETE_BY_ID_ERROR_MESSAGE = "Error deleting issue with id.";

    private final String FIND_BY_TITLE_ERROR_MESSAGE = "Error finding issue by title.";

    private final String NO_ID_OBTAINED_ERROR_MESSAGE = "Creating issue failed, no ID obtained.";

    private final String NO_ROWS_AFFECTED_ERROR_MESSAGE = "Creating issue failed, no rows affected.";

    private static final String DELETE_QUERY = "delete from t_issue where id = ?";

    private static final String UPDATE_QUERY = "update t_issue set title = ?, up_vote = ?, down_vote = ?, description=? where id = ?";

    private static final String INSERT_QUERY = "insert into t_issue (title, up_vote, down_vote, address, description) values (?, ?, ?, ?, ?)";

    private static final String SELECT_ALL_QUERY = "select id, title, up_vote, down_vote, address, description, created_at, updated_at from t_issue";

    private static final String SELECT_BY_ID_QUERY = "select id, title, up_vote, down_vote, address, description, created_at, updated_at from t_issue where id = ?";

    private static final String SELECT_BY_TITLE_QUERY = "select id, title, up_vote, down_vote, address, description, created_at, updated_at from t_issue where title = ?";

    private final DataSource dataSource;
    private final IssueRowMapper issueRowMapper;

    @Override
    public Issue save(Issue issue) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, issue.getTitle());
            preparedStatement.setInt(2, issue.getUpVote());
            preparedStatement.setInt(3, issue.getDownVote());
            preparedStatement.setString(4, issue.getAddress());
            preparedStatement.setString(5, issue.getDescription());

            if (preparedStatement.executeUpdate() == 0) {
                throw new SQLException(NO_ROWS_AFFECTED_ERROR_MESSAGE);
            }

            try (final ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return this.findById(generatedKeys.getLong(1));
                } else {
                    throw new SQLException(NO_ID_OBTAINED_ERROR_MESSAGE);
                }
            }
        } catch (SQLException exception) {
            log.error(CREATE_ERROR_MESSAGE, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_CREATED);
        }
    }

    @Override
    public Issue update(Long id, Issue issue) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, issue.getTitle());
            preparedStatement.setInt(2, issue.getUpVote());
            preparedStatement.setInt(3, issue.getDownVote());
            preparedStatement.setString(4, issue.getDescription());
            preparedStatement.setLong(5, id);
            if (preparedStatement.executeUpdate() == 0) {
                throw new SQLException(UPDATE_ERROR_MESSAGE);
            }
            return findById(id);
        } catch (SQLException exception) {
            log.error(exception.getMessage());
            throw new ApiException(ExceptionType.RESOURCE_NOT_UPDATED);
        }
    }

    @Override
    public List<Issue> findAll() {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                final List<Issue> issues = new ArrayList<>();
                while (resultSet.next()) {
                    issues.add(issueRowMapper.mapRow(resultSet, 0));
                }
                return issues;
            }
        } catch (SQLException exception) {
            log.error(FIND_ALL_ERROR_MESSAGE);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public Issue findById(Long id) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID_QUERY)) {

            preparedStatement.setLong(1, id);

            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return issueRowMapper.mapRow(resultSet, 0);
                } else {
                    throw new SQLException(FIND_BY_ID_ERROR_MESSAGE);
                }
            }

        } catch (SQLException exception) {
            log.error(FIND_BY_ID_ERROR_MESSAGE);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public Issue findByTitle(String title) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_TITLE_QUERY)) {

            preparedStatement.setString(1, title);

            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return issueRowMapper.mapRow(resultSet, 0);
                } else {
                    throw new SQLException(FIND_BY_TITLE_ERROR_MESSAGE);
                }
            }

        } catch (SQLException exception) {
            log.error(FIND_BY_TITLE_ERROR_MESSAGE);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (final SQLException exception) {
            log.error(DELETE_BY_ID_ERROR_MESSAGE + " " + id);
            throw new ApiException(ExceptionType.RESOURCE_NOT_DELETED);
        }
    }
}
