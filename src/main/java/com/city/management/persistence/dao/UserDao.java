package com.city.management.persistence.dao;

import java.util.List;
import com.city.management.persistence.entity.User;
import com.city.management.persistence.entity.FieldValueExists;

public interface UserDao extends FieldValueExists {

    List<User> findAll();

    User save(User user);

    User findByFieldNameAndValue(String fieldName, Object value);

    boolean isEmailAlreadyUsed(String email);

    User findByEmail(String email);

    User update(User user);

    User changeStatus(String email, String status);

    void destroyByEmail(String email);

}
