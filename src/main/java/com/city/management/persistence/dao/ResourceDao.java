package com.city.management.persistence.dao;

import java.util.List;
import com.city.management.persistence.entity.Resource;

public interface ResourceDao {
    Resource save(Resource resource);
    Resource findById(Long id);
    List<Resource> findAll();
    void deleteById(Long id);
    Resource update(Long id, Resource resource);
}
