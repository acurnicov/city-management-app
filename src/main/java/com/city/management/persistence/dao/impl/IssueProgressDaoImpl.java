package com.city.management.persistence.dao.impl;

import java.util.List;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Connection;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import com.city.management.exception.ApiException;
import com.city.management.exception.ExceptionType;
import com.city.management.persistence.dao.IssueProgressDao;
import com.city.management.persistence.entity.IssueProgress;
import com.city.management.persistence.rowmapper.IssueProgressRowMapper;

@Slf4j
@Repository
@RequiredArgsConstructor
@EqualsAndHashCode()

public class IssueProgressDaoImpl implements IssueProgressDao {

    private static final String INSERT_ISSUE_PROGRESS = "INSERT INTO t_issue_progress (description) VALUES (?)";
    private static final String UPDATE_ISSUE_PROGRESS = "UPDATE t_issue_progress SET description = ?, status = ?::issue_status WHERE id = ?";
    private static final String DELETE_ISSUE_PROGRESS_BY_ID = "UPDATE t_issue_progress SET status = ?::issue_status WHERE id = ?";
    private static final String FIND_ISSUE_PROGRESS_BY_ID = "SELECT * FROM t_issue_progress WHERE id = ?";
    private static final String FIND_ALL_ISSUE_PROGRESS = "SELECT * FROM t_issue_progress";

    private final DataSource dataSource;
    private final IssueProgressRowMapper issueProgressRowMapper;

    @Override
    public IssueProgress save(IssueProgress issueProgress) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ISSUE_PROGRESS, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, issueProgress.getDescription());

            if (preparedStatement.executeUpdate() == 0) {
                throw new SQLException("Creating issue progress failed, no rows affected.");
            }
            try (final ResultSet generatedKey = preparedStatement.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    return this.findById(generatedKey.getLong(1));
                } else {
                    throw new SQLException("Creating issue progress failed, no ID obtained.");
                }
            }
        } catch (SQLException exception) {
            log.error("Error executing the query.", exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_CREATED);
        }
    }

    @Override
    public IssueProgress update(Long id, IssueProgress issueProgress) {

        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ISSUE_PROGRESS)) {

            preparedStatement.setString(1, issueProgress.getDescription());
            preparedStatement.setString(2, issueProgress.getStatus().toString());
            preparedStatement.setLong(3, id);

            if (preparedStatement.executeUpdate() == 0) {
                throw new RuntimeException("Can not update issue progress");
            }

            return findById(id);
        } catch (SQLException exception) {
            log.error("Error executing the query.", exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_UPDATED);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ISSUE_PROGRESS_BY_ID)) {

            preparedStatement.setObject(1, "ABORTED");
            preparedStatement.setLong(2, id);

            preparedStatement.executeUpdate();

        } catch (SQLException exception) {
            log.error("Error executing the query.", exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public IssueProgress findById(Long id) {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(FIND_ISSUE_PROGRESS_BY_ID)) {

            preparedStatement.setLong(1, id);
            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return issueProgressRowMapper.mapRow(resultSet, 0);
                }
                throw new SQLException("Error finding issue progress by id.");
            }
        } catch (SQLException exception) {
            log.error("Error executing the query.", exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }

    }

    @Override
    public List<IssueProgress> findAll() {
        try (final Connection connection = dataSource.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_ISSUE_PROGRESS)) {
            List<IssueProgress> issueProgresses = new ArrayList<>();
            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    issueProgresses.add(issueProgressRowMapper.mapRow(resultSet, 0));
                }
                return issueProgresses;
            } catch (SQLException exception) {
                throw new SQLException("Error finding all issue progresses");
            }
        } catch (SQLException exception) {
            log.error("Error executing the query.", exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }
}
