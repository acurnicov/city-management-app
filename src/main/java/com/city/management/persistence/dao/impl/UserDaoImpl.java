package com.city.management.persistence.dao.impl;

import java.sql.Types;
import java.util.List;
import java.util.Objects;
import java.sql.ResultSet;
import java.sql.Statement;
import lombok.AccessLevel;
import java.util.ArrayList;
import java.sql.Connection;
import javax.sql.DataSource;
import java.sql.SQLException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.sql.PreparedStatement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import com.city.management.exception.ApiException;
import com.city.management.exception.ExceptionType;
import com.city.management.persistence.entity.User;
import com.city.management.persistence.dao.UserDao;
import com.city.management.persistence.rowmapper.UserRowMapper;

@Slf4j
@Repository
@RequiredArgsConstructor
public class UserDaoImpl implements UserDao {

    private static final String FIND_ALL = "select * from t_user";
    private static final String FORCE_DESTROY_BY_EMAIL = String.format("delete from t_user where %s = ?", User.DBColumn.EMAIL);
    private static final String FIND_BY_EMAIL = String.format("select * from t_user where %s = ?", User.DBColumn.EMAIL);
    private static final String FIND_BY_FIELD_NAME_AND_VALUE = "select * from t_user where ? = ?";
    private static final String UPDATE_STATUS_BY_EMAIL = String.format("update t_user set %s = ? where %s = ?", User.DBColumn.STATUS, User.DBColumn.EMAIL);
    private static final String INSERT_QUERY = String.format(
            "insert into t_user (%s, %s, %s, %s, %s, %s) values (?, ?, ?, ?, ?, ?)",
            User.DBColumn.FIRST_NAME, User.DBColumn.LAST_NAME, User.DBColumn.EMAIL, User.DBColumn.PASSWORD, User.DBColumn.PHONE_NUMBER, User.DBColumn.ADDRESS
    );
    private static final String UPDATE_QUERY = String.format(
            "update t_user set %s = ?,  %s = ?, %s = ?, %s = ? where %s = ?",
            User.DBColumn.FIRST_NAME, User.DBColumn.LAST_NAME, User.DBColumn.PHONE_NUMBER, User.DBColumn.ADDRESS, User.DBColumn.EMAIL
    );

    private final DataSource dataSource;
    private final UserRowMapper userRowMapper;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Message {

        final static String FAILED_GET_USERS                                     = "Can not get all users;";
        final static String FAILED_FIND_BY_FIELD_NAME_AND_VALUE                  = "Can not find user by %s = %s;";
        final static String FAILED_TO_SAVE_NO_ROWS_AFFECTED                      = "Creating user failed, no rows affected.";
        final static String FAILED_TO_SAVE_NO_ID                                 = "Creating user failed, no ID obtained.";
        final static String FAILED_TO_SAVE                                       = "Can not insert user;";
        final static String FAILED_TO_UPDATE_NO_ROWS_AFFECTED                    = "Updating user failed, no rows affected.";
        final static String FAILED_TO_UPDATE                                     = "Can not update user;";
        final static String FAILED_TO_UPDATE_STATUS_NO_ROWS_AFFECTED             = "Updating user status failed, no rows affected.";
        final static String FAILED_TO_UPDATE_STATUS                              = "Can not update user status;";
        final static String DELETED_ITEM                                         = "Deleted user with email = %s.";
        final static String FAILED_TO_DELETE                                     = "Can not find to delete user by email = %s;";

    }

    @Override
    public User findByFieldNameAndValue(final String fieldName, final Object value) {
        // TODO : refactoring
        final String query = FIND_BY_FIELD_NAME_AND_VALUE.replace("? = ?", fieldName + " = ?");
        final String error = String.format(Message.FAILED_FIND_BY_FIELD_NAME_AND_VALUE, fieldName, value);
        try (
                final Connection connection = dataSource.getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement(query);
        ) {
            preparedStatement.setObject(1, value);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return userRowMapper.mapRow(resultSet, 0);
            }

            log.error(error);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        } catch (SQLException exception) {
            log.error(error, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public boolean fieldValueExists(final String fieldName, final Object value) {
        return Objects.nonNull(findByFieldNameAndValue(fieldName, value));
    }

    @Override
    public List<User> findAll() {
        try (
                final Connection connection = dataSource.getConnection();
                final ResultSet resultSet = connection.prepareStatement(FIND_ALL).executeQuery();
        ) {
            final List<User> list = new ArrayList<>();
            while (resultSet.next()) {
                list.add(userRowMapper.mapRow(resultSet, 0));
            }

            return list;
        } catch (SQLException exception) {
            log.error(Message.FAILED_GET_USERS, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public User save(final User user) {
        try (
                final Connection connection = dataSource.getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getAddress());

            if (preparedStatement.executeUpdate() == 0) {
                log.error(Message.FAILED_TO_SAVE_NO_ROWS_AFFECTED);
                throw new ApiException(ExceptionType.RESOURCE_NOT_CREATED);
            }
            try ( final ResultSet generatedKeys = preparedStatement.getGeneratedKeys() ) {
                if (generatedKeys.next()) {
                    return findByFieldNameAndValue(User.DBColumn.ID, generatedKeys.getLong(User.DBColumn.ID));
                }
            }

            log.error(Message.FAILED_TO_SAVE_NO_ID);
            throw new ApiException(ExceptionType.RESOURCE_NOT_CREATED);
        } catch (SQLException exception) {
            log.error(Message.FAILED_TO_SAVE, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_CREATED);
        }
    }

    @Override
    public boolean isEmailAlreadyUsed(final String email) {
        try {
            return Objects.nonNull(findByEmail(email));
        } catch (Exception exception) {
            log.error(String.format(Message.FAILED_FIND_BY_FIELD_NAME_AND_VALUE, "email", email), exception);

            return false;
        }
    }

    @Override
    public User findByEmail(final String email) {
        final String error = String.format(Message.FAILED_FIND_BY_FIELD_NAME_AND_VALUE, "email", email);
        try (
                final Connection connection = dataSource.getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_EMAIL);
        ) {
            preparedStatement.setString(1, email);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return userRowMapper.mapRow(resultSet, 0);
            }

            log.error(error);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        } catch (SQLException exception) {
            log.error(error, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public User update(final User user) {
        try (
                final Connection connection = dataSource.getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
        ) {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getPhoneNumber());
            preparedStatement.setString(4, user.getAddress());
            preparedStatement.setString(5, user.getEmail());

            if (preparedStatement.executeUpdate() == 0) {
                log.error(Message.FAILED_TO_UPDATE_NO_ROWS_AFFECTED);
                throw new SQLException(Message.FAILED_TO_UPDATE_NO_ROWS_AFFECTED);
            }

            return findByEmail(user.getEmail());
        } catch (SQLException exception) {
            log.error(Message.FAILED_TO_UPDATE, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_UPDATED);
        }
    }

    @Override
    public User changeStatus(final String email, final String status) {
        try (
                final Connection connection = dataSource.getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STATUS_BY_EMAIL);
        ) {
            preparedStatement.setObject(1, status, Types.OTHER);
            preparedStatement.setString(2, email);

            if (preparedStatement.executeUpdate() == 0) {
                log.error(Message.FAILED_TO_UPDATE_STATUS_NO_ROWS_AFFECTED);
                throw new SQLException(Message.FAILED_TO_UPDATE_STATUS_NO_ROWS_AFFECTED);
            }

            return findByEmail(email);
        } catch (SQLException exception) {
            log.error(Message.FAILED_TO_UPDATE_STATUS, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_UPDATED);
        }
    }

    @Override
    public void destroyByEmail(final String email) {
        final String error = String.format(Message.FAILED_TO_DELETE, email);
        try (
                final Connection connection = dataSource.getConnection();
                final PreparedStatement preparedStatement = connection.prepareStatement(FORCE_DESTROY_BY_EMAIL);
        ) {
            preparedStatement.setString(1, email);
            final int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected != 0) {
                log.info(String.format(Message.DELETED_ITEM, email));
            } else {
                throw new RuntimeException(error);
            }
        } catch (SQLException exception) {
            log.error(error, exception);
            throw new ApiException(ExceptionType.RESOURCE_NOT_DELETED);
        }
    }

}
