package com.city.management.persistence.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import com.city.management.enums.IssueStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.city.management.persistence.entity.IssueProgress;

@Component
public class IssueProgressRowMapper implements RowMapper <IssueProgress> {

    @Override
    public IssueProgress mapRow(ResultSet resultSet, int i) throws SQLException {
        return IssueProgress.builder()
                .id(resultSet.getLong(IssueProgress.DBColumn.ID))
                .description(resultSet.getString(IssueProgress.DBColumn.DESCRIPTION))
                .status(IssueStatus.valueOf(resultSet.getString(IssueProgress.DBColumn.STATUS)))
                .createdAt(resultSet.getObject(IssueProgress.DBColumn.CREATED_AT, OffsetDateTime.class))
                .updatedAt(resultSet.getObject(IssueProgress.DBColumn.UPDATED_AT, OffsetDateTime.class))
                .build();
    }
}
