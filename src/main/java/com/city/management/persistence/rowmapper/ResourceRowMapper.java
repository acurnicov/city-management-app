package com.city.management.persistence.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.city.management.persistence.entity.Resource;

@Component
public class ResourceRowMapper implements RowMapper<Resource> {
    @Override
    public Resource mapRow(ResultSet resultSet, int i) throws SQLException {
        return Resource.builder()
                .id(resultSet.getLong(Resource.DBColumn.ID))
                .resourceType(resultSet.getString(Resource.DBColumn.RESOURCE_TYPE))
                .resource(resultSet.getBytes(Resource.DBColumn.RESOURCE))
                .createdAt(resultSet.getObject(Resource.DBColumn.CREATED_AT, OffsetDateTime.class))
                .build();
    }
}
