package com.city.management.persistence.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.city.management.persistence.entity.Issue;

@Component
public class IssueRowMapper implements RowMapper<Issue> {

    @Override
    public Issue mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        return Issue.builder()
                .id(resultSet.getLong(Issue.DBColumn.ID))
                .title(resultSet.getString(Issue.DBColumn.TITLE))
                .upVote(resultSet.getInt(Issue.DBColumn.UP_VOTE))
                .downVote(resultSet.getInt(Issue.DBColumn.DOWN_VOTE))
                .address(resultSet.getString(Issue.DBColumn.ADDRESS))
                .description(resultSet.getString(Issue.DBColumn.DESCRIPTION))
                .createdAt(resultSet.getObject(Issue.DBColumn.CREATED_AT, OffsetDateTime.class))
                .updatedAt(resultSet.getObject(Issue.DBColumn.UPDATED_AT, OffsetDateTime.class))
                .build();
    }

}

