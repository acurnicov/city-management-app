package com.city.management.persistence.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import com.city.management.enums.UserStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.city.management.persistence.entity.User;

@Component
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(final ResultSet resultSet, final int index) throws SQLException {
        return User.builder()
                .id(resultSet.getLong(User.DBColumn.ID))
                .firstName(resultSet.getString(User.DBColumn.FIRST_NAME))
                .lastName(resultSet.getString(User.DBColumn.LAST_NAME))
                .email(resultSet.getString(User.DBColumn.EMAIL))
                .password(resultSet.getString(User.DBColumn.PASSWORD))
                .phoneNumber(resultSet.getString(User.DBColumn.PHONE_NUMBER))
                .address(resultSet.getString(User.DBColumn.ADDRESS))
                .status(UserStatus.valueOf(resultSet.getString(User.DBColumn.STATUS)))
                .createdAt(resultSet.getObject(User.DBColumn.CREATED_AT, OffsetDateTime.class))
                .build();
    }

}
