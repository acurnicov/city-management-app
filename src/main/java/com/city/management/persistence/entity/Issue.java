package com.city.management.persistence.entity;

import lombok.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"title", "address", "description"})
public class Issue {

    private Long id;
    private String title;
    private Integer upVote;
    private Integer downVote;
    private String address;
    private String description;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;


    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class DBColumn {

        public final static String ID = "id";
        public final static String TITLE = "title";
        public final static String UP_VOTE = "up_vote";
        public final static String DOWN_VOTE = "down_vote";
        public final static String ADDRESS = "address";
        public final static String DESCRIPTION = "description";
        public final static String CREATED_AT = "created_at";
        public final static String UPDATED_AT = "updated_at";

    }
}
