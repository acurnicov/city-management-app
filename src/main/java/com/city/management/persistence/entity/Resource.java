package com.city.management.persistence.entity;

import lombok.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "resource"})
public class Resource {
    private Long id;
    private String resourceType;
    private byte[] resource;
    private OffsetDateTime createdAt;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class DBColumn {
        public static final String ID = "id";
        public static final String RESOURCE_TYPE = "resource_type";
        public static final String RESOURCE = "resource";
        public static final String CREATED_AT = "created_at";
    }
}
