package com.city.management.persistence.entity;

public interface FieldValueExists {

    boolean fieldValueExists(String fieldName, Object value);

}
