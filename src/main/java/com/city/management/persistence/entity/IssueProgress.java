package com.city.management.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;

import java.time.OffsetDateTime;
import com.city.management.enums.IssueStatus;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class IssueProgress {

    private Long id;
    private String description;
    private IssueStatus status;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class DBColumn {

        public final static String ID = "id";
        public final static String DESCRIPTION = "description";
        public final static String STATUS = "status";
        public final static String CREATED_AT = "created_at";
        public final static String UPDATED_AT = "updated_at";

    }

}


