package com.city.management.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import java.time.OffsetDateTime;
import com.city.management.enums.UserStatus;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {
    "firstName", "lastName", "email"
})
public class User {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phoneNumber;
    private String address;
    private UserStatus status;
    private OffsetDateTime createdAt;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class DBColumn {

        public final static String ID               = "id";
        public final static String FIRST_NAME       = "first_name";
        public final static String LAST_NAME        = "last_name";
        public final static String EMAIL            = "email";
        public final static String PASSWORD         = "password";
        public final static String PHONE_NUMBER     = "phone_number";
        public final static String ADDRESS          = "address";
        public final static String STATUS           = "status";
        public final static String CREATED_AT       = "created_at";

    }

}
