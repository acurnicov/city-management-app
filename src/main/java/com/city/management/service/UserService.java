package com.city.management.service;

import java.util.List;
import com.city.management.web.dto.UserDto;
import com.city.management.persistence.entity.FieldValueExists;

public interface UserService extends FieldValueExists {

    List<UserDto> getAll();

    UserDto signUp(UserDto userDto);

    boolean isEmailAlreadyUsed(String email);

    UserDto getByEmail(String email);

    UserDto update(UserDto userDto);

    UserDto changeStatus(UserDto userDto);

    void destroyByEmail(String email);

}
