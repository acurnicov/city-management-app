package com.city.management.service;

import java.util.List;
import com.city.management.web.dto.IssueProgressDto;

public interface IssueProgressService {

    IssueProgressDto save(IssueProgressDto issueProgressDto);
    IssueProgressDto update(Long id, IssueProgressDto issueProgressDto);
    void deleteById(Long id);
    IssueProgressDto findById(Long id);
    List<IssueProgressDto> findAll();

}
