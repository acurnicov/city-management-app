package com.city.management.service;

import java.util.List;
import com.city.management.web.dto.IssueDto;

public interface IssueService {
    IssueDto save(IssueDto issueDto);

    List<IssueDto> findAllIssues();

    IssueDto findById(Long id);

    IssueDto findByTitle(String title);

    IssueDto updateById(Long id, IssueDto issueDto);

    void deleteById(Long id);
}
