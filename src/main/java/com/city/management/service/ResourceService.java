package com.city.management.service;

import java.util.List;
import com.city.management.web.dto.ResourceDto;

public interface ResourceService {
    ResourceDto save(ResourceDto resourceDto);
    ResourceDto findById(Long id);
    List<ResourceDto> findAll();
    void deleteById(Long id);
    ResourceDto update(Long id, ResourceDto resourceDto);
}
