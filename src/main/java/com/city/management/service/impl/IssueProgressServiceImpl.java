package com.city.management.service.impl;

import com.city.management.persistence.dao.IssueProgressDao;
import com.city.management.persistence.entity.IssueProgress;
import com.city.management.service.IssueProgressService;
import com.city.management.web.dto.IssueProgressDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class IssueProgressServiceImpl implements IssueProgressService {

    private final IssueProgressDao issueProgressDao;

    private final ConversionService conversionService;

    @Override
    public IssueProgressDto save(final IssueProgressDto issueProgressDto){
        IssueProgress issueProgress = conversionService.convert(issueProgressDto, IssueProgress.class);
        return conversionService.convert(issueProgressDao.save(issueProgress), IssueProgressDto.class);
    }

    @Override
    public IssueProgressDto update(final Long id, final IssueProgressDto issueProgressDto) {
        IssueProgress issueProgress = conversionService.convert(issueProgressDto, IssueProgress.class);
        return conversionService.convert(issueProgressDao.update(id, issueProgress), IssueProgressDto.class);
    }

    @Override
    public void deleteById(final Long id) {
        issueProgressDao.deleteById(id);
    }

    @Override
    public IssueProgressDto findById(final Long id) {
        return conversionService.convert(issueProgressDao.findById(id), IssueProgressDto.class);
    }

    @Override
    public List<IssueProgressDto> findAll() {
       return issueProgressDao.findAll()
               .stream()
               .map(issueProgress -> conversionService.convert(issueProgress, IssueProgressDto.class))
               .collect(Collectors.toList());
    }
}
