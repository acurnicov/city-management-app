package com.city.management.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import com.city.management.web.dto.UserDto;
import org.springframework.stereotype.Service;
import com.city.management.service.UserService;
import com.city.management.persistence.entity.User;
import com.city.management.persistence.dao.UserDao;
import org.springframework.core.convert.ConversionService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    private final ConversionService conversionService;

    @Override
    public boolean fieldValueExists(final String fieldName, final Object value) throws UnsupportedOperationException {
        Objects.requireNonNull(fieldName);
        Objects.requireNonNull(value);

        return userDao.fieldValueExists(fieldName, value);
    }

    @Override
    public List<UserDto> getAll() {
        return userDao.findAll().stream()
                .map(item -> conversionService.convert(item, UserDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDto signUp(final UserDto userDto) {
        return conversionService.convert(userDao.save(conversionService.convert(userDto, User.class)), UserDto.class);
    }

    @Override
    public boolean isEmailAlreadyUsed(final String email) {
        return userDao.isEmailAlreadyUsed(email);
    }

    @Override
    public UserDto getByEmail(final String email) {
        return conversionService.convert(userDao.findByEmail(email), UserDto.class);
    }

    @Override
    public UserDto update(final UserDto userDto) {
        return conversionService.convert(userDao.update(conversionService.convert(userDto, User.class)), UserDto.class);
    }

    @Override
    public UserDto changeStatus(final UserDto userDto) {
        return conversionService.convert(userDao.changeStatus(userDto.getEmail(), userDto.getStatus().toString()), UserDto.class);
    }

    @Override
    public void destroyByEmail(final String email) {
        userDao.destroyByEmail(email);
    }

}
