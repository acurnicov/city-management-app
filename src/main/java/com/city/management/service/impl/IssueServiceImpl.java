package com.city.management.service.impl;

import com.city.management.persistence.dao.IssueDao;
import com.city.management.persistence.entity.Issue;
import com.city.management.service.IssueService;
import com.city.management.web.dto.IssueDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class IssueServiceImpl implements IssueService {

    private final IssueDao issueDao;

    private final ConversionService conversionService;

    @Override
    public IssueDto save(final IssueDto issueDto) {
        return conversionService.convert(issueDao.save(conversionService.convert(issueDto, Issue.class)), IssueDto.class);
    }

    @Override
    public List<IssueDto> findAllIssues() {
        return issueDao.findAll().stream()
                .map(item -> conversionService.convert(item, IssueDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public IssueDto findById(final Long id) {
        return conversionService.convert(issueDao.findById(id), IssueDto.class);
    }

    @Override
    public IssueDto findByTitle(final String title) {
        return conversionService.convert(issueDao.findByTitle(title), IssueDto.class);
    }

    @Override
    public IssueDto updateById(final Long id, final IssueDto issueDto) {
        return conversionService.convert(issueDao.update(id, conversionService.convert(issueDto, Issue.class)), IssueDto.class);
    }

    @Override
    public void deleteById(final Long id) {
        issueDao.deleteById(id);
    }

}
