package com.city.management.service.impl;

import com.city.management.persistence.dao.ResourceDao;
import com.city.management.persistence.entity.Resource;
import com.city.management.service.ResourceService;
import com.city.management.web.dto.ResourceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ResourceServiceImpl implements ResourceService {
    private final ResourceDao resourceDao;
    private final ConversionService conversionService;

    @Override
    public ResourceDto findById(final Long id) {
        final Resource resource = resourceDao.findById(id);
        return conversionService.convert(resource, ResourceDto.class);
    }

    @Override
    public List<ResourceDto> findAll() {
        return resourceDao.findAll().stream()
                .map(resource -> conversionService.convert(resource, ResourceDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ResourceDto save(final ResourceDto resourceDto) {
        final Resource resource = resourceDao.save(conversionService.convert(resourceDto, Resource.class));
        return conversionService.convert(resource, ResourceDto.class);
    }

    @Override
    public void deleteById(final Long id) {
        resourceDao.deleteById(id);
    }

    @Override
    public ResourceDto update(final Long id, final ResourceDto resourceDto) {
        final Resource resource = resourceDao.update(id, conversionService.convert(resourceDto, Resource.class));
        return conversionService.convert(resource, ResourceDto.class);
    }
}
