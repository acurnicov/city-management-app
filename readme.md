###City Management App

###Local App configurations
Please create a file `application.yml` in `{path_to_app}/src/resources/` app folder;
An example, please view `{path_to_app}/src/resources/application-example.yml`.

###Flyway configurations
Please create a file `flyway.properties` in `{path_to_app}/` app folder;
The required fields:
`
- flyway.user=
- flyway.password=
- flyway.schemas=
- flyway.url=
`
